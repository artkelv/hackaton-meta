<h1>Case Meta</h1>

<h2>Sobre</h2>
<p>Olá a todos. Nesta documentação você acompanha algumas das informações deste case que fizemos para a empresa Meta. Recebemos a missão de criarmos um app de feedbacks que fosse possível está a par de todas as avaliações que foram feitas pelos colaboradores da empresa. Abaixo colocamos informações sobre projeto, onde foi hospedado, integrantes e mais. Espero que gostem, se quiserem compartilhar, comentar e deixar feedbacks fiquem a vontade, será um prazer recebê-los. Desde já deixo um obrigado do time que ajudou e fez acontecer o projeto. ♥</p>
</hr>

<strong>ATENÇÃO: para visualizar as páginas que o projeto tem ultilize desta forma: /home, /register/contributors, /register/Leaguers... e assim vai. Se desejar olhar as outras páginas, peço por gentileza que verifique a pasta "Router" do nosso projeto, estará na pasta de Front-end. obrigado.</strong>

<h5>Link do projeto:<a href="https://old-fashioned-stretchh.surge.sh/">Clique aqui</a></h5>


<h4>Desenvolvedores Front-end</h4>
<p>Gledson - GitHub: <a href="https://github.com/GledsonLucas111/GledsonLucas111">Link</a></p>
<p>Geyson - GitHub: <a href="https://github.com/GeysonMes">Link</a></p>

<h4>Desenvolvedores Back-end</h4>
<p>Vitor Duarte - GitHub: <a href="https://github.com/Vitorduarte0">Link</a></p>
<p>Arthur Kelvim - GitHub: <a href="https://github.com/artkelv">Link</a></p>
<p>Antônio - GitHub <a href="https://github.com/antoniosantos2">Link</a></p>

<h3>Tecnologias usadas</h3>

<p>React, NodeJs, MySql, TypeScript, Figma e Trello</p>

<h3>Bibliotecas usadas</h3>

<p>Styled-Components, Axios, React-Router</p>

<h3>LAYOUT DAS PAGINAS</h3>

<p>Decidimos que iriamos trabalhar com a ferramenta Figma para que pudessemos fazer a prototipação das páginas, também usamos a ferramenta Trello para que pudessemos organizar todas as funcionalidades por tarefas no aplicativo. O projeto é um tanto robusto e infelizmente nao foi finalizado.</p>

<h3>PALETA DE CORES</h3>

<h4>Cores Principais</h4>
<p>Branca, o branco incita clareza e inocência.</p>
<p>Azul, o azul traz paz e harmonia.</p>


<h3>REQUISITOS</h3>

<p>Dividindo a criação dos componentes entre os menbros da equipe. Usando a ferramenta "Trello".</p>

<p>Protótipo do projeto: <a href="https://www.figma.com/file/FPxgEGfi97al972MAFxt7g/Front-end-Hackaton?node-id=0%3A1">Clique aqui</a></p>

<a href="https://old-fashioned-stretchh.surge.sh/">Visualizar Projeto</a>
