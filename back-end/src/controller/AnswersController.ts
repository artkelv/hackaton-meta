import {Request, Response} from "express"
import { CustomError } from "../error/CustomError"
import { AnswersBusiness } from "../business/AnswersBusiness"

export class AnswersController {
    constructor(private answersBusiness:AnswersBusiness){}
    async AnswersByFeedback(
        req:Request,
        res:Response
    ){
        try {
            const token = req.headers.authorization as string 
            const result = await this.answersBusiness.AnswersByFeedback(token)
            res.status(200).send(result)
        } catch (error:any) {
            if(error instanceof CustomError){
                res.status(error.statusCode).send({message:error.message})
            }else if(error){
                res.status(400).send(error.message)
            }else{
                res.status(500).send("Erro ao consultar o servidor")
            }
        }
    }
}