import { Request, Response } from "express";
import { UserBusiness } from "../business/UserBusiness";
import { CustomError } from "../error/CustomError";
import { loginDTO, signupDTO } from "../model/userDTO";

export class UserController {
  constructor(private userBusiness: UserBusiness) {}
  signupController = async (req: Request, res: Response): Promise<void> => {
    try {
      const { name, email, password, role } = req.body;
      const infoUserToSignup: signupDTO = {
        ContributorsName: name,
        email,
        password,
        role
      };
      const token = await this.userBusiness.signupBusiness(infoUserToSignup);
      res
        .status(201)
        .send({ message: `Usuário(a) ${name} cadastrado com sucesso!`, token });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      } else {
        res.status(500).send({ message: "Erro ao se conectar com o servidor" });
      }
    }
  };
  loginController = async (req: Request, res: Response): Promise<void> => {
    try {
      const { email, password } = req.body;
      const loginDTO: loginDTO = {
        email,
        password
      };
      const token = await this.userBusiness.loginBusiness(loginDTO);
      res.status(200).send({ token });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      } else {
        res.status(500).send({ message: "Erro ao se conectar com o servidor" });
      }
    }
  };
  usersController = async (req: Request, res: Response): Promise<void> => {
    try {
      const token = req.headers.authorization as string;
      const getContributors = await this.userBusiness.userBusiness(token);
      res.status(200).send({ contributors: getContributors });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      } else {
        res.status(500).send({ message: "Erro ao se conectar com o servidor" });
      }
    }
  };
}
