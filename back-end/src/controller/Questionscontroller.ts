import { Request, Response } from "express";
import { QuestionsBusiness } from "../business/QuestionsBusiness";
import { CustomError } from "../error/CustomError";

export class QuestionsController {
  constructor(private questionsBusiness: QuestionsBusiness) {}
  getQuestionsController = async (
    req: Request,
    res: Response
  ): Promise<void> => {
    try {
      const token = req.headers.authorization as string;
      const questions = await this.questionsBusiness.getQuestionsBusiness(
        token
      );
      res.status(200).send({ questions });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      } else {
        res.status(500).send({ message: "Erro ao se conectar com o servidor" });
      }
    }
  };
}
