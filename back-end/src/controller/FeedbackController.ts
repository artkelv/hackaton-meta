import { Request, Response } from "express";
import { FeedBackBusiness } from "../business/FeedbackBusiness";
import { CustomError } from "../error/CustomError";
import { involvedDTO } from "../types/email";


export class FeedBackController {
    constructor(private feedBackBusiness: FeedBackBusiness){}
  async sendEmailToInvolved(req: Request, res: Response):Promise<void> {
      try {
          const email = req.body.email
          const email1 = req?.body.email1
          const email2 = req?.body.email2
          const email3 = req?.body.email3
          const token = req.headers.authorization as string
          const emails = {
            email, email1, email2, email3
          } 
          await this.feedBackBusiness.sendEmailBusiness(emails, token)
          res.status(200).send({message: "Avaliação enviada com sucesso!"})
      } catch (error) {
        if (error instanceof CustomError) {
            res.status(error.statusCode).send({ message: error.message });
          } else if (error) {
            res.status(400).send({ message:error });
          } else {
            res.status(500).send({ message: "Erro ao se conectar com o servidor" });
          }
      }
  }

 async createInvolvedController(req: Request, res: Response): Promise<void>{
   try {
     const {name, company, charge} = req.body
     const infoInvolved: involvedDTO = {
      name, company, charge
     }
     await this.feedBackBusiness.createInvolvedBusiness(infoInvolved)
     res.status(201).send({message: "avaliador criado com sucesso"})
   } catch (error) {
     if (error instanceof CustomError) {
            res.status(error.statusCode).send({ message: error.message });
          } else if (error) {
            res.status(400).send({ message:error });
          } else {
            res.status(500).send({ message: "Erro ao se conectar com o servidor" });
          }
      }
 }
}
