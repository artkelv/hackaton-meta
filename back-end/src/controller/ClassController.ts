import { Request, Response } from "express";
import { CustomError } from "../error/CustomError";
import { ClassBusiness } from "../business/ClassBusiness";
import { InputClassDTO } from "../model/Class";

export class ClassController {
  constructor(private classBusiness: ClassBusiness) {}
  async createClass(req: Request, res: Response) {
    try {
      const { ClassName, phase_turma, contributors_id } = req.body;
      const token = req.headers.authorization as string;

      const inputClassDTO: InputClassDTO = {
        ClassName,
        phase_turma,
        contributors_id
      };

      const result = await this.classBusiness.createClass(inputClassDTO, token);

      res.status(result.code).send({ message: result.message });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ error: error });
      } else {
        res.status(500).send("Error ao acessar o servidor");
      }
    }
  }
  async getClasses(req: Request, res: Response): Promise<void> {
    try {
      const token = req.headers.authorization as string;
      const classes = await this.classBusiness.classes(token);
      res.status(200).send({ classes });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ error: error });
      } else {
        res.status(500).send("Error ao acessar o servidor");
      }
    }
  }
}
