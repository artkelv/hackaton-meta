import { query, Request, Response } from "express";
import { LeagueBusiness } from "../business/LeagueBusiness";
import { CustomError } from "../error/CustomError";
import { leaguerDTO } from "../types/Leaguer";
export class LeagueController {
  constructor(private leagueBusiness: LeagueBusiness) {}
  createLeaguer = async (req: Request, res: Response): Promise<void> => {
    try {
      const {
        name,
        email,
        class_id,
        contributors_id,
        tecnologies,
        language
      } = req.body;
      const photo = req?.body.photo
      const token = req.headers.authorization as string;
      const leaguerDTO: leaguerDTO = {
        name,
        email,
        class_id,
        contributors_id,
        photo,
        tecnologies,
        language
      };
      const leaguer = await this.leagueBusiness.createLeague(leaguerDTO, token);
      res
        .status(201)
        .send({ message: `${name}, cadastrado com sucesso!`, leaguer });
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      }else{
        res.status(500).send({message: "Erro ao se conectar com o servidor"})
      }
    }
  };
  getLeaguerController = async (req: Request, res: Response): Promise<void> => {
    try {
      const token = req.headers.authorization as string
      const pages = req.query.pages
      const leaguers = await this.leagueBusiness.getLeaguersBusiness(token, Number(pages))
      res.status(200).send({leaguers}) 
    } catch (error) {
      if(error instanceof CustomError){
        res.status(error.statusCode).send({message: error.message})
      }else if(error){
        res.status(400).send({message: error})
      }else{
        res.status(500).send({message: "Erro ao se conectar com o servidor"})
      }
    }
  };
  editLeaguerController = async (req: Request, res: Response): Promise<void> => {
    try {
      const {name, email, class_id, contributors_id,tecnologies, language} = req.body
      const leaguerDTO: leaguerDTO = {
        name,
        email,
        class_id,
        contributors_id,
        tecnologies,
        language
      };
        const id = req.params.id
        const token = req.headers.authorization as string;
         await this.leagueBusiness.editLeaguerBusiness(leaguerDTO, token, id);
      res
        .status(201)
        .send({ message: `Os dados do ${name}, foram atualizados com sucesso!`});
    } catch (error) {
      if (error instanceof CustomError) {
        res.status(error.statusCode).send({ message: error.message });
      } else if (error) {
        res.status(400).send({ message: error });
      }else{
        res.status(500).send({message: "Erro ao se conectar com o servidor"})
      }
    }
  }
  async deleteLeguerById(
    req:Request,
    res:Response
  ):Promise<void>{
    try {
      const id:string = req.params.id
      const token = req.headers.authorization as string
      
      const result = await this.leagueBusiness.deleteLeguerById(id, token)

      res.status(200).send({message:result})
    } catch (error) {
      if(error instanceof CustomError){
        res.status(error.statusCode).send({message:error.message})
      } else if(error){
        res.status(400).send({error:error})
      } else{
        res.status(500).send("Error ao conectar com o servidor")
      }

    }
  }
  detailLeaguerController = async (req: Request, res: Response): Promise<void> => {
    try {
      const token = req.headers.authorization as string
      const idLeaguer  = req.params.idLeaguer
     
      const leaguer = await this.leagueBusiness.datailLeaguerBusiness(token, idLeaguer)
      res.status(200).send({leaguer})
    } catch (error) {
      if(error instanceof CustomError){
        res.status(error.statusCode).send({message:error.message})
      } else if(error){
        res.status(400).send({error:error})
      } else{
        res.status(500).send("Error ao conectar com o servidor")
      }
    }
  }
}
