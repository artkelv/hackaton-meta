import { LeagueDataBase } from "../data/LeagueDataBase";
import { CustomError } from "../error/CustomError";
import { ILeaguerDataBase } from "../model/ILeaguerDataBase";
import { IUserDataBase } from "../model/IUserDataBase";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";
import { leaguer, leaguerDTO, leaguerUpdate } from "../types/Leaguer";

export class LeagueBusiness {
  constructor(
    private idGeneration: IdGenerator,
    private leaguerDataBase: ILeaguerDataBase,
    private authenticator: Authenticator
  ) {}
  createLeague = async (
    leaguer: leaguerDTO,
    token: string
  ): Promise<leaguer> => {
    const {
      name,
      email,
      class_id,
      contributors_id,
      photo,
      tecnologies,
      language
    } = leaguer;
    if (
      !name ||
      !email ||
      !class_id ||
      !contributors_id ||
      !tecnologies ||
      !language
    ) {
      throw new CustomError(
        422,
        "Por favor verifique se todos os campos estão preenchidos"
      );
    }
    const isVerifyExistLeaguer = await this.leaguerDataBase.getLeaguerByEmail(
      email
    );
    if (isVerifyExistLeaguer) {
      throw new CustomError(401, "Esse leaguer já existe");
    }
    const tokenData = this.authenticator.getTokenData(token);

    if (tokenData.role !== "ADM" && tokenData.role !== "MENTOR") {
      throw new CustomError(
        403,
        "Somente administradores e mentores podem cadastrar um leaguer"
      );
    }

    const id = this.idGeneration.generateId();
    const createLeague: leaguer = {
      id,
      name_leguer: name,
      email,
      class_id,
      contributors_id,
      photo,
      tecnologies,
      language
    };

    const leaguerBd = await this.leaguerDataBase.createLeaguer(createLeague);
    return leaguerBd;
  };
  getLeaguersBusiness = async (
    token: string,
    pages: number
  ): Promise<leaguer[]> => {
    if (!token) {
      throw new CustomError(422, "É necessário informar o token de acesso");
    }
    if (pages < 1) {
      throw new CustomError(404, "Não é possível numero 0 e numeros positivos");
    }
    const tokenData = this.authenticator.getTokenData(token);
    const quantifyLeguers: number = 6;
    const offset: number = quantifyLeguers * (pages - 1);

    const leaguers = await this.leaguerDataBase.getLeaguersData(
      tokenData.id,
      tokenData.role,
      offset
    );
    if (leaguers.length <= 0) {
      throw new CustomError(404, "Página não encontrada");
    }
    return leaguers;
  };

  async deleteLeguerById(id: string, token: string): Promise<string> {
    if (id.length < 4) {
      throw new CustomError(422, "O campo de id não foi preenchido.");
    }
    if (!token) {
      throw new CustomError(422, "É necessário informar um token.");
    }

    const verifyIdLeguer = await this.leaguerDataBase.getLeguerByID(id);

    if (!verifyIdLeguer) {
      throw new CustomError(400, "Esse leguer não existe");
    }
    const tokenData = this.authenticator.getTokenData(token);

    if (tokenData.role !== "ADM") {
      throw new CustomError(403, "Somente ADMs conseguem excluir um leguer.");
    }
    const deleteLeguer = await this.leaguerDataBase.deleteLeguer(id);

    if (!deleteLeguer) {
      throw new CustomError(400, "Não foi possivel deletar esse leguer");
    }
    return "Leguer deletado com sucesso";
  }
  editLeaguerBusiness = async (
    leaguerDTO: leaguerDTO,
    token: string,
    id: string
  ): Promise<void> => {
    const {
      name,
      email,
      photo,
      class_id,
      contributors_id,
      tecnologies,
      language
    } = leaguerDTO;
    if (
      !name ||
      !email ||
      !class_id ||
      !contributors_id ||
      !tecnologies ||
      !language
    ) {
      throw new CustomError(
        422,
        "Por favor verifique se todos os campos foram preenchidos!"
      );
    }
    if (!token) {
      throw new CustomError(401, "Por favor insira um token de acesso!");
    }
    const verifyExistLeaguer = await this.leaguerDataBase.getLeaguerById(id);
    if (!verifyExistLeaguer) {
      throw new CustomError(404, "Esse leaguers não existe!");
    }
    const tokenData = this.authenticator.getTokenData(token);
    if (tokenData.role === "GESTOR") {
      throw new CustomError(
        403,
        "Somente mentores e Administradores poderão editar um leaguers"
      );
    }
    const leaguerToUpdate: leaguerUpdate = {
      name_leguer: name,
      email,
      class_id,
      contributors_id,
      photo,
      tecnologies,
      language
    };

    await this.leaguerDataBase.editLeaguerBusiness(id, leaguerToUpdate);
  };

  datailLeaguerBusiness = async (token: string, id: string) => {
    if (id.length < 4) {
      throw new CustomError(422, "O campo de id não foi preenchido.");
    }
    if (!token) {
      throw new CustomError(422, "É necessário informar um token.");
    }
    const tokenData = this.authenticator.getTokenData(token);
    const verifyIdLeguer = await this.leaguerDataBase.getLeguerByID(id);
    if (!verifyIdLeguer) {
      throw new CustomError(400, "Esse leguer não existe");
    }
    const leaguer = await this.leaguerDataBase.getLeaguerByIdTypeRole(
      tokenData.id,
      tokenData.role,
      id
    );
    return leaguer;
  };
}
