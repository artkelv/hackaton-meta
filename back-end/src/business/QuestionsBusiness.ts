import { CustomError } from "../error/CustomError";
import { IQuestionsData } from "../model/IQuestionsData";
import { Authenticator } from "../services/Authenticator";
import { Questions } from "../types/Questions";

export class QuestionsBusiness {
  constructor(
    private authenticator: Authenticator,
    private questionsData: IQuestionsData
  ) {}
  getQuestionsBusiness = async (token: string): Promise<Questions[]> => {
    if (!token) {
      throw new CustomError(422, "É preciso informar o token de acesso");
    }
    const tokenData = this.authenticator.getTokenData(token);
    if (tokenData.role === "GESTOR") {
      throw new CustomError(
        403,
        "Somente administradores e mentores conseguem ter acesso as perguntas"
      );
    }
    const questions = await this.questionsData.getAllQuestions();
    return questions;
  };
}
