import { CustomError } from "../error/CustomError";
import { IUserDataBase } from "../model/IUserDataBase";
import { User } from "../model/IUser";

import { Users } from "../model/User";
import { Authenticator } from "../services/Authenticator";
import { HashManager } from "../services/HashGenerator";
import { IdGenerator } from "../services/IdGenerator";
import { authenticationData } from "../types/authenticationData";
import { loginDTO, signupDTO } from "../model/userDTO";
import { NodeMailer } from "../services/NodeMailer";

export class UserBusiness {
  constructor(
    private userData: IUserDataBase,
    private authenticator: Authenticator,
    private hashManager: HashManager,
    private idGeneration: IdGenerator,
    private user: Users,
    private nodemailer: NodeMailer
  ) {}
  signupBusiness = async (infoUserToSignup: signupDTO): Promise<string> => {
    const { email, ContributorsName, password, role } = infoUserToSignup;

    if (!email || !ContributorsName || !password || !role) {
      throw new CustomError(
        422,
        "Campos 'name', 'email', 'password' e 'role' não foram preenchidos!"
      );
    }
    const isVerifyExistUser = await this.userData.getUserByEmail(email);
    if (isVerifyExistUser) {
      throw new CustomError(401, "Usuário já existe");
    }
    if (password.length < 8) {
      throw new CustomError(401, "Senha deve ter no mínimo 8 caractéries");
    }
    const newRole = this.user.stringToUserRole(role.toUpperCase());

    const id = this.idGeneration.generateId();
    const newPassword = this.hashManager.hash(password);
    const userDB: User = {
      id,
      ContributorsName,
      email,
      password: newPassword,
      role: newRole
    };

    await this.userData.signupUserDataBase(userDB);
    const payload: authenticationData = {
      id,
      role: userDB.role
    };

    await this.nodemailer.sendEmailContributors(email, password);

    const accessToken = this.authenticator.generateToken(payload);
    return accessToken;
  };
  loginBusiness = async (infoToLogin: loginDTO): Promise<string> => {
    const { email, password } = infoToLogin;
    if (!email || !password) {
      throw new CustomError(
        422,
        "Os campos 'email' e 'password' não foram preenchidos!"
      );
    }
    const user = await this.userData.getUserByEmail(email);

    const isVerifyPassword = this.hashManager.compareHash(
      password,
      user.password
    );
    if (!isVerifyPassword) {
      throw new CustomError(401, "Email ou senha inválidos");
    }
    const payload: authenticationData = {
      id: user.id,
      role: user.role
    };
    const token = this.authenticator.generateToken(payload);
    return token;
  };
  userBusiness = async (token: string): Promise<User[]> => {
    const tokenData = this.authenticator.getTokenData(token);
    if (!tokenData) {
      throw new CustomError(422, "É necessário informar um token");
    }
    if (tokenData.role !== "ADM") {
      throw new CustomError(
        403,
        "Somente ADMs consegue ver todos os mentores/gestores"
      );
    }
    const users = await this.userData.getAllUser();
    return users;
  };
}
