import { FeedBackData } from "../data/FeedbackData";
import { CustomError } from "../error/CustomError";
import { IFeedbackDataBase } from "../model/IFeedbackDataBase";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";
import { NodeMailer } from "../services/NodeMailer";
import { involved, involvedDTO } from "../types/email";
import { arrEmail } from "../types/nodeMailer";

export class FeedBackBusiness {
  constructor(
    private nodeMailer: NodeMailer,
    private authenticator: Authenticator,
    private idGeneration: IdGenerator,
    private feedbackDataBase: IFeedbackDataBase
  ) {}
  sendEmailBusiness = async (
    emails: arrEmail,
    token: string
  ): Promise<void> => {
    const { email } = emails;
    if (!token) {
      throw new CustomError(422, "É necessário passar o token");
    }
    if (!email) {
      throw new CustomError(
        422,
        "É necessário informar pelo menos uma pessoa para avaliar"
      );
    }
    const tokenData = this.authenticator.getTokenData(token);
    if (!tokenData) {
      throw new CustomError(404, "Usuário deslogado!!");
    }
    const newEmails: arrEmail[] = [
      emails.email,
      emails.email1,
      emails.email2,
      emails.email3
    ] as any;
    await this.nodeMailer.sendHashLink(newEmails);
  };

  createInvolvedBusiness = async (infoInvolved: involvedDTO): Promise<void> => {
    const { name, charge, company } = infoInvolved;
    if (!name || !charge || !company) {
      throw new CustomError(
        422,
        "Por favor verifique se os campos foram preenchidos"
      );
    }

    const id = this.idGeneration.generateId();
    const newInvolved: involved = {
      id,
      name,
      company,
      charge
    };
    await this.feedbackDataBase.createInvolved(newInvolved);
  };
}
