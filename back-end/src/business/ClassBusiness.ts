import { ClassDataBase } from "../data/ClassDataBase";
import { CustomError } from "../error/CustomError";
import { InputClassDTO } from "../model/Class";
import { IClassDataBase } from "../model/IClassDataBase";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";
import { Class } from "../types/ClassType";

export class ClassBusiness {
  constructor(
    private Idgenerator: IdGenerator,
    private authenticator: Authenticator,
    private classDataBase: IClassDataBase
  ) {}
  async createClass(infoClass: InputClassDTO, token: string) {
    const { ClassName, phase_turma, contributors_id } = infoClass;
    if (!ClassName || !contributors_id) {
      throw new CustomError(
        422,
        "Os campos não foram preenchidos corretamente. Veja se não está faltando algo."
      );
    }

    const verifyToken = this.authenticator.getTokenData(token);

    if (!verifyToken) {
      throw new CustomError(
        401,
        "O usuário não está autenticado. Por gentileza faça o login novamente."
      );
    } else if (typeof verifyToken !== "object") {
      throw new CustomError(
        401,
        "O token está inválido ou encontra-se expirado. Faça o login novamente para iniciar a sessão."
      );
    } else if (verifyToken.role !== "ADM") {
      throw new CustomError(403, "Apenas Administradores podem criar turmas.");
    }

    const verifyClassExists = await this.classDataBase.classExists(ClassName);
    if (verifyClassExists) {
      throw new CustomError(302, "A turma já existe no sistema!");
    }
    const id = this.Idgenerator.generateId();
    const classe: Class = {
      id,
      ClassName,
      phase_turma,
      contributors_id
    };
    await this.classDataBase.createClass(classe);
    const result = {
      code: 201,
      message: "Turma criada com sucesso!"
    };
    return result;
  }
  async classes(token: string): Promise<Class[]> {
    if (!token) {
      throw new CustomError(422, "O token de acesso precisa ser preenchido");
    }
    const verifyToken = this.authenticator.getTokenData(token);
    if (!verifyToken) {
      throw new CustomError(
        401,
        "O usuário não está autenticado. Por gentileza faça o login novamente."
      );
    } else if (typeof verifyToken !== "object") {
      throw new CustomError(
        401,
        "O token está inválido ou encontra-se expirado. Faça o login novamente para iniciar a sessão."
      );
    } else if (verifyToken.role !== "ADM") {
      throw new CustomError(403, "Apenas Administradores podem ver as turmas.");
    }
    const getAllClasses = await this.classDataBase.classes();

    return getAllClasses;
  }
}
