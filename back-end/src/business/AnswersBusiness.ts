import { AnswersData } from "../data/AnswersData";
import { CustomError } from "../error/CustomError";
import { Authenticator } from "../services/Authenticator";

export class AnswersBusiness {
    constructor(
        private authenticator:Authenticator,
        private answersData:AnswersData
        ){}
    async AnswersByFeedback(token:string){
            if(!token){
                throw new CustomError(404, "Não foi encontrado autorização, faça o login novamente")
            }
            const auth = this.authenticator.getTokenData(token)
            if(!auth){
                throw new CustomError(400, "Voce não está autenticado. Faça o login novamente")
            }

            if(auth.role !== "ADM" && auth.role !== "GESTOR" && auth.role !== "MENTOR"){
                throw new CustomError(401, "Você não tem autorização para verificar uma avaliação")
            } 
            const returnDataEvaluators = await this.answersData.getFeedbackByEvaluators(auth.role)
            
            if(returnDataEvaluators.length < 1){
                throw new CustomError(200,"Este colaborador não solicitou nenhuma avaliação para ninguém");
            }
            
            return returnDataEvaluators
    }
}