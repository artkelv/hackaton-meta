import { IUserDataBase } from "../model/IUserDataBase";
import { User } from "../model/IUser";
import { BaseDatabase } from "./BaseDataBase";

export class UserDataBase extends BaseDatabase implements IUserDataBase {
  private TABLE_NAME: string = "Contributors";
  signupUserDataBase = async (user: User): Promise<void> => {
    await this.connection(this.TABLE_NAME).insert(user);
  };
  getUserByEmail = async (email: string): Promise<User> => {
    const user = await this.connection.from(this.TABLE_NAME).where({ email });
    return user[0];
  };
  async getAllUser(): Promise<User[]> {
    const users = await this.connection(this.TABLE_NAME);
    return users;
  }
  async getUserById(id: string): Promise<User> {
    const contributors = await this.connection
      .from(this.TABLE_NAME)
      .where({ id });
    return contributors[0];
  }
}
