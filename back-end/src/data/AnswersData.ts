import { BaseDatabase } from "./BaseDataBase";

export class AnswersData extends BaseDatabase {
    async getFeedbackByEvaluators(id:string) {
        
        const result = await this.connection.raw(`
            SELECT Answers.answer_of_involved, Involved.name, Leguers.name_leguer FROM Answers JOIN
            Involved JOIN Leguers WHERE id_of_people_solicited_evaluated = "${id}" 
            AND Answers.id_of_evaluator = Involved.id AND Answers.id_of_evaluated = Leguers.id ORDER BY name ASC;
        `)
        
        const data = result[0].map((item:any) => {
            const dataInfo = {
                nome_do_avaliado:item.name_leguer,
                resposta:item.answer_of_involved,
                nome_do_avaliador:item.name
            }
            return dataInfo
        })
        return data   
    }
}
