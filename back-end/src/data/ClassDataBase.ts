import { IClassDataBase } from "../model/IClassDataBase";
import { Class } from "../types/ClassType";
import { BaseDatabase } from "./BaseDataBase";

export class ClassDataBase extends BaseDatabase implements IClassDataBase {
  private TABLE_NAME = "Class";

  async createClass(Class: Class): Promise<void> {
    await this.connection().insert(Class).into(this.TABLE_NAME);
  }
  async classExists(className: string): Promise<Class> {
    const result = await this.connection()
      .from(this.TABLE_NAME)
      .where("ClassName", "=", className);
    return result[0];
  }
  async classes(): Promise<Class[]> {
    const classesDB = await this.connection.from(this.TABLE_NAME);
    return classesDB;
  }
}
