import { IFeedbackDataBase } from "../model/IFeedbackDataBase";
import { involved } from "../types/email";
import { BaseDatabase } from "./BaseDataBase";

export class FeedBackData extends BaseDatabase implements IFeedbackDataBase {
  private TABLE_NAME = "Involved";
  async createInvolved(involved: involved): Promise<void> {
    await this.connection(this.TABLE_NAME).insert(involved);
  }
}
