import { BaseDatabase } from "./BaseDataBase";

export class Migrations extends BaseDatabase {
    createTables(){
        this.connection.raw(`
            CREATE TABLE Contributors(
                id VARCHAR(255) PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL UNIQUE,
                password VARCHAR(255) NOT NULL,
                role ENUM("ADM", "GESTOR", "MENTOR") NOT NULL
            );
            CREATE TABLE Class(
                id VARCHAR(255) PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                phase_turma VARCHAR(255) DEFAULT "introduçao",
                contributors_id VARCHAR(255) NOT NULL,
                FOREIGN KEY(contributors_id) REFERENCES Contributors(id)
            );
            CREATE TABLE Leguers(
                id VARCHAR(255) PRIMARY KEY,
                name_leguer VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL,
                contributors_id VARCHAR(255) NOT NULL,
                class_id VARCHAR(255) NOT NULL,
                FOREIGN KEY(class_id) REFERENCES Class(id),
                FOREIGN KEY(contributors_id) REFERENCES Contributors(id)
            );
            CREATE TABLE Questions(
                id VARCHAR(255) PRIMARY KEY NOT NULL,
                question VARCHAR(255) NOT NULL
            );
            CREATE TABLE Involved(
                id VARCHAR(255)PRIMARY KEY NOT NULL,
                name VARCHAR(255) NOT NULL, 
                company VARCHAR(255) NOT NULL,
                charge VARCHAR(255) NOT NULL
            );
            CREATE TABLE Answers(
                id_of_answer VARCHAR(255) PRIMARY KEY,
                answer_of_involved VARCHAR(255) NOT NULL,
                id_of_people_solicited_evaluated VARCHAR(255) NOT NULL,
                id_of_evaluated VARCHAR(255) NOT NULL,
                id_of_question VARCHAR(255) NOT NULL,
                id_of_evaluator VARCHAR(255) NOT NULL,
                FOREIGN KEY(id_of_people_solicited_evaluated) REFERENCES Contributors(id),
                FOREIGN KEY(id_of_evaluated) REFERENCES Leguers(id),
                FOREIGN KEY(id_of_question) REFERENCES Questions(id),
                FOREIGN KEY(id_of_evaluator) REFERENCES Involved(id)
            );
            INSERT INTO  Questions (id, question)
            VALUES 
            ("1", "Projeto(s):"),
            ("2", "Tecnologias:"),
	        ("3", "Tempo de formação:"),
            ("4", "Profissional:"),
            ("5", "Papel (função):"),
            ("6", "Modelo de contratação?"),
            ("7", "Demonstrou perfomance na entrega?"),
            ("8", "Entregou com qualidade?"),
            ("9", "Profissional proativo?"),
            ("10", "Demonstrou comprometimento?"),
            ("11", "Sabe trabalhar em equipe?"),
            ("12", "Desenvovleu suas habilidades?"),
            ("13", "Possui capacidade de liderança?"),
            ("14", "Sabe trabalhar sobre pressão?"),
            ("15", "Participou ativamente das cerimônias?"),
            ("16", "Realizou atividades administrativas?"),
            ("17", "Quais as caracteristicas que o profissional se destaca?"),
            ("18", "Considerações gerais:");
        `).then(() => {
            console.log("Tabelas criadas!")
        }).catch((erro) => {
            console.log(erro.message)
        })
    }
}
const migrations = new Migrations();
migrations.createTables();

