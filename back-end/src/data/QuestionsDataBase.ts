import { IQuestionsData } from "../model/IQuestionsData";
import { Questions } from "../types/Questions";
import { BaseDatabase } from "./BaseDataBase";

export class QuestionsDataBase extends BaseDatabase implements IQuestionsData {
  private TABLE_NAME = "Questions";
  async getAllQuestions(): Promise<Questions[]> {
    const questions: Questions[] = await this.connection
      .from(this.TABLE_NAME)
      .orderBy("id", "asc", "first");
    return questions;
  }
}
