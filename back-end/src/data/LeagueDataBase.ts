import { ILeaguerDataBase } from "../model/ILeaguerDataBase";
import { leaguer } from "../types/Leaguer";
import { BaseDatabase } from "./BaseDataBase";

export class LeagueDataBase extends BaseDatabase implements ILeaguerDataBase {
  private TABLE_NAME = "Leguers";
  private TABLE_NAME1 = "Class";

  async createLeaguer(leaguer: leaguer): Promise<leaguer> {
    await this.connection(this.TABLE_NAME).insert(leaguer);
    return leaguer;
  }
  async getLeaguerByEmail(email: string): Promise<leaguer> {
    const leaguer = await this.connection
      .from(this.TABLE_NAME)
      .where({ email });
    return leaguer[0];
  }

  async getLeaguerById(id: string): Promise<leaguer> {
    const leaguer = await this.connection.from(this.TABLE_NAME).where({ id });
    return leaguer[0];
  }
  async getLeaguerByIdTypeRole(
    id: string,
    role: string,
    idLeaguer: string
  ): Promise<leaguer> {
    const getLeaguerByRole = async (id: string, role: string) => {
      if (role === "GESTOR") {
        const leaguer = await this.connection
          .from(this.TABLE_NAME)
          .join("Class", "Leguers.class_id", "=", "Class.id")
          .join(
            "Contributors",
            `Leguers.contributors_id`,
            "=",
            "Contributors.id"
          )
          .select(
            "Leguers.id",
            "Leguers.name_leguer",
            "Leguers.email",
            "Class.ClassName",
            "Class.phase_turma",
            "Contributors.ContributorsName",
            "Leguers.tecnologies",
            "Leguers.photo",
            "Leguers.language"
          )
          .where("Leguers.id", "=", `${idLeaguer}`);
        return leaguer[0];
      } else if (role !== "GESTOR") {
        const leaguer = await this.connection(this.TABLE_NAME)
          .join("Class", "Leguers.class_id", "=", "Class.id")
          .join(
            "Contributors",
            `Leguers.contributors_id`,
            "=",
            "Contributors.id"
          )
          .select(
            "Leguers.id",
            "Leguers.name_leguer",
            "Leguers.email",
            "Class.ClassName",
            "Class.phase_turma",
            "Contributors.ContributorsName",
            "Leguers.tecnologies",
            "Leguers.photo",
            "Leguers.language"
          )
          .where("Leguers.id", "=", `${idLeaguer}`);
        return leaguer[0];
      } else {
        return "erro ao encontrar leaguer";
      }
    };
    return (getLeaguerByRole(id, role) as unknown) as Promise<leaguer>;
  }

  async getLeaguersData(
    id: string,
    role: string,
    offset: number
  ): Promise<leaguer[]> {
    const getLeaguerByRole = async (
      id: string,
      role: string,
      offset: number
    ) => {
      if (role === "GESTOR") {
        const leaguer = await this.connection
          .from(this.TABLE_NAME)
          .join("Class", "Leguers.class_id", "=", "Class.id")
          .join(
            "Contributors",
            `Leguers.contributors_id`,
            "=",
            "Contributors.id"
          )
          .select(
            "Leguers.id",
            "Leguers.name",
            "Leguers.email",
            "Class.ClassName",
            "Class.phase_turma",
            "Contributors.ContributorsName",
            "Leguers.tecnologies",
            "Leguers.photo",
            "Leguers.language"
          )
          .where("Leguers.contributors_id", "=", `${id}`)
          .limit(6)
          .offset(offset);
        return leaguer;
      } else if (role !== "GESTOR") {
        const leaguer = await this.connection(this.TABLE_NAME)
          .join("Class", "Leguers.class_id", "=", "Class.id")
          .join(
            "Contributors",
            `Leguers.contributors_id`,
            "=",
            "Contributors.id"
          )
          .select(
            "Leguers.id",
            "Leguers.name_leguer",
            "Leguers.email",
            "Class.ClassName",
            "Class.phase_turma",
            "Contributors.ContributorsName",
            "Leguers.tecnologies",
            "Leguers.photo",
            "Leguers.language"
          )
          .limit(6)
          .offset(offset);

        return leaguer;
      } else {
        return "erro ao encontrar leaguer";
      }
    };
    return getLeaguerByRole(id, role, offset) as Promise<leaguer[]>;
  }

  async deleteLeguer(id: string): Promise<number> {
    const result = await this.connection()
      .delete()
      .from(this.TABLE_NAME)
      .where("id", "=", id);
    return result;
  }
  async getLeguerByID(id: string): Promise<leaguer> {
    const result = await this.connection()
      .select()
      .from(this.TABLE_NAME)
      .where("id", "=", id);
    return result[0];
  }

  async editLeaguerBusiness(
    idParamns: string,
    Leaguers: leaguer
  ): Promise<void> {
    const {
      id,
      name_leguer,
      photo,
      email,
      tecnologies,
      contributors_id,
      class_id,
      language
    } = Leaguers;
    await this.connection
      .from(this.TABLE_NAME)
      .where({ id: idParamns })
      .update({
        id,
        name_leguer,
        photo,
        email,
        tecnologies,
        contributors_id,
        class_id,
        language
      });
  }
}
