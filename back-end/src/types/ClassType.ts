export type Class = {
  id: string;
  ClassName: string;
  phase_turma: string;
  contributors_id: string;
};
