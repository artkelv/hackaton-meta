import { UserRole } from "../model/User"

export type authenticationData = {
    id:string,
    role: UserRole
}