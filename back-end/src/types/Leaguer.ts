export type leaguer = {
  id: string;
  name_leguer: string;
  email: string;
  class_id: string;
  contributors_id: string;
  photo: string | undefined;
  tecnologies: string;
  language: string;
};

export type leaguerDTO = {
  name: string;
  email: string;
  class_id: string;
  contributors_id: string;
  photo?: string;
  tecnologies: string;
  language: string;
};

export type leaguerUpdate = {
  name_leguer: string;
  email: string;
  class_id: string;
  contributors_id: string;
  photo?: string;
  tecnologies: string;
  language: string;
};

// export enum turma {
//   TURMA_PILOTO = "turma piloto",
//   TURMA_1 = "Turma 1",
//   TURMA_2 = "Turma 2",
//   TURMA_3 = "turma 3"
// }
