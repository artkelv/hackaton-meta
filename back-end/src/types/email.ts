export type arrEmail = {
  email1: string;
  email2?: string;
  email3?: string;
  email4?: string;
};
export type involvedDTO = {
  name: string,
  company: string,
  charge: string
}
export type involved = {
  id: string
  name: string,
  company: string,
  charge: string
}

