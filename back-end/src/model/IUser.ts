import { UserRole } from "./User";

export interface User {
  id: string;
  ContributorsName: string;
  email: string;
  password: string;
  role: UserRole;
}
