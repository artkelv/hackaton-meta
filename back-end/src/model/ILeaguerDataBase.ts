import { leaguer, leaguerUpdate } from "../types/Leaguer";

export interface ILeaguerDataBase {
  createLeaguer(leaguer: leaguer): Promise<leaguer>;
  getLeaguerByEmail(email: string): Promise<leaguer>;
  getLeaguerById(id: string): Promise<leaguer>;
  getLeaguersData(id: string, role: string, offset: number): Promise<leaguer[]>;
  getLeaguerByIdTypeRole(
    id: string,
    role: string,
    idLeaguer: string
  ): Promise<leaguer>;

  deleteLeguer(id: string): Promise<number>;
  getLeguerByID(id: string): Promise<leaguer>;

  editLeaguerBusiness(id: string, leaguer: leaguerUpdate): Promise<void>;
}
