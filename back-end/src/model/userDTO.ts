import { UserRole } from "./User";

export type signupDTO = {
  ContributorsName: string;
  email: string;
  password: string;
  role: UserRole;
};

export type loginDTO = {
  email: string;
  password: string;
};
