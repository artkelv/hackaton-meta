import { Questions } from "../types/Questions";

export interface IQuestionsData {
  getAllQuestions(): Promise<Questions[]>;
}
