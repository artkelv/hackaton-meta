import { User } from "./IUser";

export interface IUserDataBase {
  signupUserDataBase(user: User): Promise<void>;
  getUserByEmail(email: string): Promise<User>;
  getAllUser(): Promise<User[]>;
  getUserById(id: string): Promise<User>;
}
