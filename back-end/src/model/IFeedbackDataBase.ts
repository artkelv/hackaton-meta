import { involved } from "../types/email";

export interface IFeedbackDataBase {
  createInvolved(involved: involved): Promise<void>;
}
