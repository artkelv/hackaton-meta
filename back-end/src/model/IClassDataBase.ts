import { Class } from "../types/ClassType";

export interface IClassDataBase {
  createClass(Class: Class): Promise<void>;
  classExists(className: string): Promise<Class>;
  classes(): Promise<Class[]>
}
