export class Users {
  // constructor(
  //   private id: string,
  //   private name: string,
  //   private email: string,
  //   private password: string,
  //   private role: UserRole
  // ) {}

  // getId() {
  //   return this.id;
  // }

  // getName() {
  //   return this.name;
  // }

  // getEmail() {
  //   return this.email;
  // }

  // getPassword() {
  //   return this.password;
  // }

  // getRole() {
  //   return this.role;
  // }

  // setId(id: string) {
  //   this.id = id;
  // }

  // setName(name: string) {
  //   this.name = name;
  // }

  // setEmail(email: string) {
  //   this.email = email;
  // }

  // setPassword(password: string) {
  //   this.password = password;
  // }

  // setRole(role: UserRole) {
  //   this.role = role;
  // }

  stringToUserRole(input: string): UserRole {
    switch (input) {
      case "ADM":
        return UserRole.ADM;
      case "MENTOR":
        return UserRole.MENTOR;
      case "GESTOR":
        return UserRole.GESTOR;
      default:
        throw new Error("Invalid user role");
    }
  }
}
export enum UserRole {
  ADM = "ADM",
  MENTOR = "MENTOR",
  GESTOR = "GESTOR"
}
