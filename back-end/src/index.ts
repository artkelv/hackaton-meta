import express, { Express } from "express";
import cors from "cors";
import { AddressInfo } from "net";
import { userRouter } from "./router/userRoutes";
import { leaguerRouter } from "./router/leaguerRouter";

import { questionRouter } from "./router/questionRouter";

import { ClassRouter } from "./router/ClassRouter";
import { formsRouter } from "./router/formsRouter";
import { AnswersRouter } from "./router/answers";


const app: Express = express();

app.use(express.json());
app.use(cors());
app.use("/user", userRouter);
app.use("/leaguer", leaguerRouter);

app.use("/question", questionRouter);

app.use("/class", ClassRouter)
app.use("/forms", formsRouter)
app.use("/answers", AnswersRouter)

const server = app.listen(process.env.PORT || 3004, () => {
  if (server) {
    const address = server.address() as AddressInfo;
    console.log(`Server is running in http://localhost:${address.port}`);
  } else {
    console.error(`Failure upon starting server.`);
  }
});
