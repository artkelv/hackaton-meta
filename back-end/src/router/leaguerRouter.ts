import express from "express";
import { LeagueBusiness } from "../business/LeagueBusiness";
import { LeagueController } from "../controller/LeagueController";
import { LeagueDataBase } from "../data/LeagueDataBase";
import { UserDataBase } from "../data/UserDataBase";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";

export const leaguerRouter = express.Router();
const idGeneration = new IdGenerator();
const authenticator = new Authenticator();

const leaguerDataBase = new LeagueDataBase();
const leaguerBusiness = new LeagueBusiness(
  idGeneration,
  leaguerDataBase,
  authenticator
);
const leaguerController = new LeagueController(leaguerBusiness);

leaguerRouter.post("/create", (req, res) =>
  leaguerController.createLeaguer(req, res)
);
leaguerRouter.get("/", (req, res) =>
  leaguerController.getLeaguerController(req, res)
);
leaguerRouter.get("/:idLeaguer", (req, res) =>
  leaguerController.detailLeaguerController(req, res)
);

leaguerRouter.delete("/:id", (req, res) => {
  leaguerController.deleteLeguerById(req, res);
});

leaguerRouter.put("/edit/:id", (req, res) =>
  leaguerController.editLeaguerController(req, res)
);
