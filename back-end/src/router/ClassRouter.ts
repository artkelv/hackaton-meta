import express from "express"

import { ClassBusiness } from "../business/ClassBusiness"
import { ClassController } from "../controller/ClassController"
import { ClassDataBase } from "../data/ClassDataBase"
import { Authenticator } from "../services/Authenticator"
import { IdGenerator } from "../services/IdGenerator"

export const ClassRouter = express.Router()

const idGenerator = new IdGenerator()
const authenticator = new Authenticator()
const classDataBase = new ClassDataBase()
const classBusiness = new ClassBusiness(
    idGenerator,
    authenticator,
    classDataBase
);
const classController = new ClassController(classBusiness)

ClassRouter.post("/create", (req, res) => {
    classController.createClass(req,res)
})
ClassRouter.get("/", (req, res) => {
    classController.getClasses(req,res)
})