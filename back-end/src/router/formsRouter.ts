import express from "express";
import { FeedBackBusiness } from "../business/FeedbackBusiness";
import { FeedBackController } from "../controller/FeedbackController";
import { FeedBackData } from "../data/FeedbackData";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";
import { NodeMailer } from "../services/NodeMailer";
export const formsRouter = express.Router();
const nodeMailer = new NodeMailer();
const authenticator = new Authenticator();
const idGeneration = new IdGenerator();
const feedbackDataBase = new FeedBackData();
const feedBackBusiness = new FeedBackBusiness(
  nodeMailer,
  authenticator,
  idGeneration,
  feedbackDataBase
);
const feedBackController = new FeedBackController(feedBackBusiness);
formsRouter.post("/create", (req, res) =>
  feedBackController.sendEmailToInvolved(req, res)
);
formsRouter.post("/create/involved", (req, res) =>
  feedBackController.createInvolvedController(req, res)
);
