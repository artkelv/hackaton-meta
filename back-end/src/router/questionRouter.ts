import express from "express";
import { QuestionsBusiness } from "../business/QuestionsBusiness";
import { QuestionsController } from "../controller/Questionscontroller";
import { QuestionsDataBase } from "../data/QuestionsDataBase";
import { Authenticator } from "../services/Authenticator";

export const questionRouter = express.Router();

const questionsDataBase = new QuestionsDataBase();
const authenticator = new Authenticator();
const questionsBusiness = new QuestionsBusiness(
  authenticator,
  questionsDataBase
);

const questionsController = new QuestionsController(questionsBusiness);
questionRouter.get("/", (req, res) =>
  questionsController.getQuestionsController(req, res)
);
