import express from "express";
import { UserBusiness } from "../business/UserBusiness";
import { UserController } from "../controller/UserController";
import { UserDataBase } from "../data/UserDataBase";
import { Users } from "../model/User";

import { Authenticator } from "../services/Authenticator";
import { HashManager } from "../services/HashGenerator";
import { IdGenerator } from "../services/IdGenerator";
import { NodeMailer } from "../services/NodeMailer";
export const userRouter = express.Router();

const userData = new UserDataBase();
const authenticator = new Authenticator();
const hashManager = new HashManager();
const idGeneration = new IdGenerator();
const user = new Users();
const nodemailer = new NodeMailer();
const userBusiness = new UserBusiness(
  userData,
  authenticator,
  hashManager,
  idGeneration,
  user,
  nodemailer
);
const userController = new UserController(userBusiness);

userRouter.post("/signup", (req, res) =>
  userController.signupController(req, res)
);
userRouter.post("/login", (req, res) =>
  userController.loginController(req, res)
);
userRouter.get("/", (req, res) => userController.usersController(req, res));
