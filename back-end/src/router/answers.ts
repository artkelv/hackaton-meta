/*
port express from "express"

import { ClassBusiness } from "../business/ClassBusiness"
import { ClassController } from "../controller/ClassController"
import { ClassDataBase } from "../data/ClassDataBase"
import { Authenticator } from "../services/Authenticator"
import { IdGenerator } from "../services/IdGenerator"

export const ClassRouter = express.Router()

const idGenerator = new IdGenerator()
const authenticator = new Authenticator()
const classDataBase = new ClassDataBase()
const classBusiness = new ClassBusiness(
    idGenerator,
    authenticator,
    classDataBase
);
const classController = new ClassController(classBusiness)

ClassRouter.post("/create", (req, res) => {
    classController.createClass(req,res)
})
ClassRouter.get("/", (req, res) => {
    classController.getClasses(req,res)

*/
import express from "express"

import { AnswersBusiness } from "../business/AnswersBusiness"
import { AnswersController } from "../controller/AnswersController"
import { AnswersData } from "../data/AnswersData"
import { Authenticator } from "../services/Authenticator"

export const AnswersRouter = express.Router()

const authenticator = new Authenticator()
const answersData = new AnswersData()
const answersBusiness = new AnswersBusiness(
    authenticator,
    answersData
)

const answerController = new AnswersController(answersBusiness)

AnswersRouter.get("/feedbacks", (req, res) => {
    answerController.AnswersByFeedback(req,res)
})