import * as bcrypt from "bcryptjs";

export class HashManager {
  hash = (plainText: string): string => {
    const rounds = Number(process.env.BCRYPT_COST);
    const cypherText: string = bcrypt.hashSync(plainText, rounds);
    return cypherText;
  };

  compareHash = (plainText: string, cypherText: string): boolean => {
    return bcrypt.compareSync(plainText, cypherText);
  };
}
