import nodemailer from "nodemailer";

import { arrEmail } from "../types/nodeMailer";

export class NodeMailer {
  async sendHashLink(arrEmail: arrEmail[]) {
    const NMAILER_CONFIG = {
      host: "smtp-mail.outlook.com",
      port: 587,
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS
    };

    const transporter = nodemailer.createTransport({
      host: NMAILER_CONFIG.host,
      port: NMAILER_CONFIG.port,
      secure: false,
      auth: {
        user: NMAILER_CONFIG.user,
        pass: NMAILER_CONFIG.pass
      },
      tls: {
        rejectUnauthorized: false
      }
    });

    await transporter.sendMail({
      text: "Segue o link para o feedback <link_do_formulario>",
      subject: "Avaliação Meta",
      from: "TESTE HACKATON META <vitormeta2022@outlook.com>",
      to: [`${arrEmail[0]},${arrEmail[1]},${arrEmail[2]},${arrEmail[3]}`]
    });
  }

  async sendEmailContributors(email: string, password: string) {
    const NMAILER_CONFIG = {
      host: "smtp-mail.outlook.com",
      port: 587,
      user: process.env.NODEMAILER_USER,
      pass: process.env.NODEMAILER_PASS
    };

    const transporter = nodemailer.createTransport({
      host: NMAILER_CONFIG.host,
      port: NMAILER_CONFIG.port,
      secure: false,
      auth: {
        user: NMAILER_CONFIG.user,
        pass: NMAILER_CONFIG.pass
      },
      tls: {
        rejectUnauthorized: false
      }
    });

    await transporter.sendMail({
      text: `Usuário Cadastro Com Sucesso ✔`,
      subject: "Credenciais",
      from: "ENVIO DE CREDENCIAIS <vitormeta2022@outlook.com>",
      html: `<p>- EQUIPE META. Segue ao lado seus dados para login na plataforma:<strong> ${email} e ${password}</strong></p>`,
      to: [`${email}`]
    });
  }
}
/* const nodemailerTeste = new NodeMailer()
.sendHashLink() */
