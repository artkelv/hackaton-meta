import axios from "axios";
import { alerts } from "../constants/alerts";
import { BASE_URL } from "../constants/urls";
import { goToHome } from "../routes/coordinator";

export const onSubmitLogin = (form, clearFields, navigate) => {
  const body = {
    email: form.email,
    password: form.password,
  };
  axios
    .post(`${BASE_URL}/user/login`, body)
    .then((res) => {
      localStorage.setItem("token", res.data.token);
      goToHome(navigate);
      clearFields();
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};

export const resisterContributor = (form, clearFields) => {
  const body = {
    name: form.name,
    email: form.email,
    password: form.password,
    role: form.role,
  };
  axios
    .post(`${BASE_URL}/user/signup`, body, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      alerts("success", `${res.data.message}`);
      clearFields();
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};

export const resisterInvolved = (form, clearFields) => {
  const body = {
    name: form.name,
    company: form.company,
    charge: form.charge,
  };
  axios
    .post(`${BASE_URL}/forms/create/involved`, body, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      alerts("success", `${res.data.message}`);
      clearFields();
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};

export const registerClass = (form, clearFields) => {
  const body = {
    ClassName: form.name,
    phase_turma: form.phase,
    contributors_id: form.contributors,
  };
  axios
    .post(`${BASE_URL}/class/create`, body, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      alerts("success", `${res.data.message}`);
      clearFields();
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};
export const registerLeaguers = (form, clearFields) => {
  const body = {
    name: form.name,
    email: form.email,
    tecnologies: form.tecnologies,
    contributors_id: form.contributors,
    class_id: form.class,
    language: form.language,
  };

  axios
    .post(`${BASE_URL}/leaguer/create`, body, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      alerts("success", `${res.data.message}`);
      clearFields();
      
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};

export const deleteLeaguer = (id,setVersion) => {
  axios
    .delete(`${BASE_URL}/leaguer/${id}`, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
    .then((res) => {
      alerts("success", `${res.data.message}`);
      setVersion();
    })
    .catch((e) => {
      alerts("error", `${e.response.data.message}`);
    });
};

export const editLeaguer =(id, form, clearFields, setVersion)=> {
  const body = {
    name: form.name,
    email: form.email,
    tecnologies: form.tecnologies,
    contributors_id: form.contributors,
    class_id: form.class_id,
    language: form.language,
  };
  
    axios
      .put(`${BASE_URL}/leaguer/edit/${id}`, body, {
        headers: {
          Authorization: localStorage.getItem("token"),
        },
      })
      .then((res) => {
        alerts("success", `${res.data.message}`);
        clearFields();
        setVersion();
      })
      .catch((e) => {
        alerts("error", `${e.response.data.message}`);
      });
  
};
