import React from "react";
import { Container, Form, Input, SubContainer } from "./styled";
import { Menu } from "../../components/menu/index";
import { Header } from "../../components/header/index";

const Register = () => {
  return (
    <div>
      <Header />
      <Container>
        <Menu />
        <SubContainer>
          <Form>
            <Input>
              <input type="text" placeholder="Nome"></input>
              <select>
                <option value="cargo" disabled>
                  Cargo
                </option>
                <option value="cargo">Administrador</option>
                <option value="cargo">Gestor</option>
                <option value="cargo">Mentor</option>
              </select>
              <input type="text" placeholder="E-mail"></input>
              <input type="text" placeholder="Senha"></input>
            </Input>
            <button>CADASTRAR</button>
          </Form>
        </SubContainer>
      </Container>
    </div>
  );
};
export default Register;
