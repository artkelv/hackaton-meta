import styled from "styled-components";
import background from "../../assets/background.svg";

export const Container = styled.div`
  display: grid;
  grid-template-columns: 200px 1fr;
`;
export const SubContainer = styled.div`
  display: flex;
  align-items: center;
  justify-items: center;
  width: 100%;
  height: 91.5vh;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-size: cover;
`;
export const Form = styled.div`
  display: flex;
  background: white;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 80vw;
  height: 80vh;
  border-radius: 9px 9px 9px 9px;
  margin-left: 5%;
  button {
    background: #122870;
    color: white;
    border-style: outset;
    border-color: #122870;
    height: 40px;
    width: 390px;
    font: bold 15px Open Sans;
    text-shadow: none;
    margin-top: 40px;
    :hover {
      cursor: pointer;
    }
  }
`;
export const Input = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 30px;
  input {
    width: 35vw;
    height: 7vh;
    border: 1px solid #000;
    background: #fff;
  }
  select {
    background: #fff;
  }
`;
