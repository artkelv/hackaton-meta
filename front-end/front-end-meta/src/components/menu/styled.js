import { Box } from "@mui/material";
import styled from "styled-components";

export const Container = styled(Box)`
  position: absolute;
  top: 13px;
  width: 100px;
  @media screen and (min-width: 230px) and (max-width: 600px) {
    top: 10px;
  }
`;

export const MenuHome = styled(Box)`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 20px 0px 0px 10px;
  &:focus {
    background: green;
  }
`;
