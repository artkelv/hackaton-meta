import React from "react";
import { Container, MenuHome } from "./styled";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import HomeIcon from "@mui/icons-material/Home";
import GroupsIcon from "@mui/icons-material/Groups";
import { useNavigate } from "react-router-dom";
import {
  goToRegisterContributors,
  goToRegisterClass,
  goToRegisterLeaguers,
  goToProfileContributor,
  goToHistoricFeedback,
} from "../../routes/coordinator";
import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import List from "@mui/material/List";
import MenuIcon from "@mui/icons-material/Menu";
import HistoryIcon from "@mui/icons-material/History";

export const Menu = () => {
  const navigate = useNavigate();
  const [state, setState] = React.useState({ left: false });
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <MenuHome>
          <HomeIcon color="neutral" />
          <Button
            sx={{ width: "84%", justifyContent: "flex-start" }}
            color="neutral"
            onClick={() => goToProfileContributor(navigate)}
          >
            Home
          </Button>
        </MenuHome>
        <div>
          <Accordion
            expanded={expanded === "panel1"}
            onChange={handleChange("panel1")}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <GroupsIcon color="neutral" />
              <Typography
                sx={{ width: "5%", flexShrink: 0, marginLeft: "5px" }}
                color="neutral"
              >
                CADASTRO
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box sx={{ display: "flex", flexDirection: "column" }}>
                <Button onClick={() => goToRegisterContributors(navigate)}>
                  Colaborador
                </Button>
                <Button onClick={() => goToRegisterLeaguers(navigate)}>
                  Leaguer
                </Button>
                <Button onClick={() => goToRegisterClass(navigate)}>
                  Turma
                </Button>
              </Box>
            </AccordionDetails>
          </Accordion>
        </div>
        <MenuHome>
          <HistoryIcon color="neutral" />
          <Button
            sx={{ width: "84%", justifyContent: "flex-start" }}
            color="neutral"
            onClick={() => goToHistoricFeedback(navigate)}
          >
            Histórico de feedback
          </Button>
        </MenuHome>
      </List>
    </Box>
  );

  return (
    <Container>
      <div>
        {["left"].map((anchor) => (
          <React.Fragment key={anchor}>
            <Button onClick={toggleDrawer(anchor, true)}>
              <MenuIcon color="primary2" />
            </Button>
            <SwipeableDrawer
              anchor={anchor}
              open={state[anchor]}
              onClose={toggleDrawer(anchor, false)}
              onOpen={toggleDrawer(anchor, true)}
            >
              {list(anchor)}
            </SwipeableDrawer>
          </React.Fragment>
        ))}
      </div>
    </Container>
  );
};
