import { Typography, Card } from "@mui/material";
import { Box } from "@mui/system";
import styled from "styled-components";
import { neutralColor } from "../../constants/colors";

export const CardLeague = styled(Card)`
  min-width: 350px;
  height: 152px;
  display: grid;
  grid-template-columns: 120px 1fr 80px;
  align-items: center;
  justify-items: start;
  border-radius: 10px;
  margin: 0px 0px 10px 0px;

  @media screen and (min-width: 320px) and (max-width: 439px) {
    min-width: 330px;
    grid-template-columns: 100px 1fr 80px;
  }
`;
export const InfosLeague = styled(Typography)`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 3px;
  margin: 5px 0px 5px;
  color: ${neutralColor};
`;

export const BoxLeague = styled(Box)`
  height: 80px;
  width: 80px;
  object-fit: cover;
  margin: 0px 0px 0px 23px;
  @media screen and (min-width: 320px) and (max-width: 439px) {
    margin: 0px 0px 0px 15px;
  }
`;
