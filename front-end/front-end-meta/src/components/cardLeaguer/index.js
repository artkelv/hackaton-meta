import {
  Box,
  Button,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import React from "react";
import VisibilityIcon from "@mui/icons-material/Visibility";
import GroupsIcon from "@mui/icons-material/Groups";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import { BoxLeague, CardLeague, InfosLeague } from "./styled";
import { goToProfileLeaguer } from "../../routes/coordinator";
import { useNavigate } from "react-router-dom";
import user from "../../assets/user.png";
export const CardLeaguer = ({ id, name, photo, Class, phase, Contributor }) => {
  const navigate = useNavigate();

  const goLeaguerDetail = (id) => {
    goToProfileLeaguer(navigate, id);
    console.log(id);
  };
  return (
    <CardLeague>
      <BoxLeague>
        <CardMedia
          sx={{ borderRadius: "50%" }}
          component="img"
          height="80px"
          image={photo === null ? `${user}` : photo}
          alt={name}
        />
      </BoxLeague>

      <Box>
        <CardContent sx={{ gap: "100px" }}>
          <Typography
            variant="h6"
            color="secondary"
            fontWeight="semi-bold"
            fontSize="17px"
          >
            {name}
          </Typography>

          <InfosLeague variant="body2">
            <GroupsIcon color="neutral" />
            {Class}
          </InfosLeague>

          <InfosLeague variant="body2">
            <MenuBookIcon color="neutral" />
            {phase}
          </InfosLeague>

          <InfosLeague variant="body2">
            <AssignmentIndIcon color="neutral" />
            {Contributor}
          </InfosLeague>
        </CardContent>
      </Box>

      <Box>
        <CardActions>
          <Button size="small" onClick={() => goLeaguerDetail(id)}>
            <VisibilityIcon />
          </Button>
        </CardActions>
      </Box>
    </CardLeague>
  );
};
