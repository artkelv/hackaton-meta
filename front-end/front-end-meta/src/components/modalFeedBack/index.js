import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import { TextField, Typography } from "@mui/material";
import { style, buttonSave, BoxInputs, BoxModalStyle } from "./styled";
import UseForm from "../../hooks/UseForm";
import { getPayload } from "../../constants/transformToken";
import useRequestData from "../../hooks/UseRequestData";

export const ModalFeedback = ({ openFeedback, setOpenFeedback, email }) => {
  const [data] = useRequestData([], "/user");
  const handleClose = () => setOpenFeedback(false);
  const { form, onChange, clearFields } = UseForm({
    professional: email,
    contributor: getPayload().id,
    coWorker1: "",
    coWorker2: "",
    coWorker3: "",
    client: "",
  });

  const getContributor =
    data.contributors &&
    data.contributors.filter(
      (contributor) => contributor.id === getPayload().id
    );

  return (
    <div>
      <Modal
        open={openFeedback}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <BoxModalStyle sx={style}>
          <Typography variant="h4" color="primary">
            Avaliação
          </Typography>

          <BoxInputs>
            <TextField
              name="professional"
              onChange={onChange}
              value={form.professional}
              id="outlined-basic"
              label="Profissional"
              variant="outlined"
              defaultValue={email}
            />
            <TextField
              name="coWorker1"
              onChange={onChange}
              value={form.coWorker1}
              id="outlined-basic"
              label="Colega 1"
              variant="outlined"
            />
            <TextField
              name="contributor"
              value={form.contributor}
              onChange={onChange}
              id="outlined-basic"
              label="Gestor ou resposável"
              variant="outlined"
              defaultValue={getContributor?.[0].email}
            />
            <TextField
            name="coWorker2"
            onChange={onChange}
            value={form.coWorker2}
              id="outlined-basic"
              label="Colega 2"
              variant="outlined"
            />
            <TextField 
            name="client"
            onChange={onChange}
            value={form.client}
            id="outlined-basic" 
            label="Cliente" 
            variant="outlined" />
            <TextField
            name="coWorker3"
            onChange={onChange}
            value={form.coWorker3}
              id="outlined-basic"
              label="Colega 3"
              variant="outlined"
            />
          </BoxInputs>

          <Box>
            <Button onClick={handleClose} sx={buttonSave}>
              Criar
            </Button>
            <Button variant="contained" sx={buttonSave}>
              Ir para Formulario
            </Button>
          </Box>
        </BoxModalStyle>
      </Modal>
    </div>
  );
};
