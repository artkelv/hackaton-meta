import { Box } from "@mui/material";
import styled from "styled-components";
import { primaryColor, PrimaryColor2 } from "../../constants/colors";

export const BoxModalStyle = styled(Box)`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 60%;
  height: 65%;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  @media screen and (min-width: 500px) and (max-width: 1200px) {
    height: 70%;
    width: 80%;
  }
  @media screen and (min-width: 200px) and (max-width: 499px) {
    justify-content: space-evenly;
    height: 90%;
    width: 90%;
  }
`;
export const style = {
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export const BoxInputs = styled(Box)`
  width: 100%;
  height: 50%;
  display: grid;
  gap: 20px;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(3, 60px);
  align-items: center;
  @media screen and (min-width: 200px) and (max-width: 555px) {
    grid-template-columns: 1fr;
    height: 100%;
    align-items: start;
  }
`;

export const buttonSave = {
  width: "200px",
  marginLeft: "10px",
  color: `${PrimaryColor2}`,
  backgroundColor: `${primaryColor}`,
  "&:hover": {
    color: `${PrimaryColor2}`,
    backgroundColor: `${primaryColor}`,
  },
};
