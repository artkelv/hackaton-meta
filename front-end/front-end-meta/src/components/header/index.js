import React from "react";
import { AppBar, IconButton, Menu, MenuItem, Toolbar } from "@mui/material";
import logo from "../../assets/logo.png";
import Box from "@mui/material/Box";
import {
  goToHome,
  goToLogin,
  goToProfileContributor,
} from "../../routes/coordinator";
import { useNavigate } from "react-router-dom";
import { AccountCircle } from "@mui/icons-material";
import { HeaderBar } from "./styled";

export const Header = () => {
  const navigate = useNavigate();
  const [auth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    goToLogin(navigate);
    localStorage.removeItem("token");
  };
  const home = () => {
    goToHome(navigate);
  };
  const profileContributor = () => {
    goToProfileContributor(navigate);
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="secondary">
        <HeaderBar>
          <Toolbar sx={{ cursor: "pointer", margin: "0px 0px 0px 30px" }}>
            <img src={logo} alt="logo" width="80px" onClick={() => home()} />
          </Toolbar>
          <Toolbar>
            {auth && (
              <div>
                <IconButton onClick={handleMenu} color="inherit">
                  <AccountCircle />
                </IconButton>

                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{ vertical: "top", horizontal: "right" }}
                  keepMounted
                  transformOrigin={{ vertical: "top", horizontal: "right" }}
                  open={Boolean(anchorEl)}
                  onClose={handleClose}
                >
                  <MenuItem onClick={() => profileContributor()}>
                    Profile
                  </MenuItem>
                  <MenuItem onClick={() => logout()}>Logout</MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </HeaderBar>
      </AppBar>
    </Box>
  );
};
