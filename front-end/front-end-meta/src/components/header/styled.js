import { Toolbar } from "@mui/material";
import styled from "styled-components";

export const HeaderBar = styled(Toolbar)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
