import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import TablePagination from "@mui/material/TablePagination";
import { Button } from "@mui/material";
import PersonRemoveIcon from "@mui/icons-material/PersonRemove";
import useRequestData from "../../hooks/UseRequestData";
import { deleteLeaguer } from "../../services/requests";
import GlobalStateContext from "../../global/GlobalStateContext";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#D55E00",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export const CustomizedTables = () => {
  const [page, setPage] = React.useState(0);
  const {version, setVersion} = React.useContext(GlobalStateContext)
  const Leaguer = useRequestData([], "/leaguer", version);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const updateState = () =>
  setVersion((version) => version + 1);

  const delLeaguer = (id) => {
    deleteLeaguer(id, updateState)
  };

  return (
    <TableContainer component={Paper} sx={{ width: "100%", height: "90%" }}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Nome</StyledTableCell>
            <StyledTableCell>E-mail</StyledTableCell>
            <StyledTableCell>Turma</StyledTableCell>

            <StyledTableCell align="right">Deletar</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Leaguer[0]?.leaguers === undefined? <p>Sem leaguer cadastrados</p> : 
          Leaguer[0]?.leaguers &&
            Leaguer[0]?.leaguers.map((league) => {
              return (
                <StyledTableRow key={league.id}>
                  <StyledTableCell component="th" scope="row">
                    {league.name_leguer}
                  </StyledTableCell>
                  <StyledTableCell>{league.email}</StyledTableCell>
                  <StyledTableCell>{league.ClassName}</StyledTableCell>
                  <StyledTableCell align="right">
                    <Button onClick={() => delLeaguer(league.id)}>
                      <PersonRemoveIcon sx={{ color: "#D55E00" }} />
                    </Button>
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
        </TableBody>
      </Table>
      <TablePagination
        component="div"
        count={Leaguer.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
};
