export const goToLogin = (navigate) => {
  navigate(`/`);
};
export const goToHome = (navigate) => {
  navigate(`/home`);
};
export const goToProfileContributor = (navigate) => {
  navigate(`/contributor`);
};

export const goToProfileLeaguer = (navigate, id) => {
  navigate(`/leaguer/${id}`);
};

export const goToRegisterContributors = (navigate) => {
  navigate(`/register/contributors`);
};
export const goToRegisterLeaguers = (navigate) => {
  navigate(`/register/leaguers`);
};
export const goToRegisterClass = (navigate) => {
  navigate(`/register/class`);
};
export const goToHistoricFeedback = (navigate) => {
  navigate(`/historic/feedbacks`);
};
