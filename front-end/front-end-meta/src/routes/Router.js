import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "../pages/home/Home";
import ProfileLeaguer from "../pages/profileLeaguer/ProfileLeaguer";
import Login from "../pages/login/Login";
import RegisterContributors from "../pages/register/RegisterContributors";
import RegisterLeaguers from "../pages/register/RegisterLeaguers";
import ProfileContributor from "../pages/profileContributor/ProfileContributor";
import RegisterClass from "../pages/register/RegisterClass";
import Involved from "../pages/involved/Involved";
import Form from "../pages/form/Form";
import Historic from "../pages/profileLeaguer/historicLeaguer/Historic";
import HistoricFeedbacks from "../pages/historicFeedbacks/index";
const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login />} />

        <Route exact path="/home" element={<Home />} />

        <Route
          exact
          path="/register/contributors"
          element={<RegisterContributors />}
        />

        <Route exact path="/register/Leaguers" element={<RegisterLeaguers />} />

        <Route exact path="/register/Class" element={<RegisterClass />} />

        <Route path="/contributor" element={<ProfileContributor />} />

        <Route path="/historic/feedbacks" element={<HistoricFeedbacks />} />

        <Route path="/leaguer/:id" element={<ProfileLeaguer />} />

        <Route path="/feedback" element={<Involved />} />

        <Route path="/feedback/formulario" element={<Form />} />

        <Route path="/historico" element={<Historic />} />
      </Routes>
    </BrowserRouter>
  );
};
export default Router;
