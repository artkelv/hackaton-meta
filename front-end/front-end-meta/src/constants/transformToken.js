export const getPayload = () => {
    const token = localStorage.getItem("token")
    var payload = token.split('.')[1];
    return JSON.parse(window.atob(payload));
};