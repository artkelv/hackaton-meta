import { createGlobalStyle } from "styled-components";
import { backgroundColor } from "../constants/colors";

export default createGlobalStyle`
  *{
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      font-family: 'Open Sans', sans-serif;
  }
  html,body{
    background-color: ${backgroundColor};
  }

`;
