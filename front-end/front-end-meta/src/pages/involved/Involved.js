import { Box, Button, CardMedia, Container } from "@mui/material";
import * as React from "react";
import {ContainerInvolved,styleContainer,buttonFB,styleInputs,TextInput,Form,} from "./styled";
import parte from "../../assets/parte.svg";
import UseForm from "../../hooks/UseForm";
import { resisterInvolved } from "../../services/requests";
import UseProtectPage from "../../hooks/UseProtectPage";

const Involved = () => {
  const { form, onChange, clearFields } = UseForm({
    name: "",
    company: "",
    charge: "",
  });
  UseProtectPage();

  const submitInvolved = (e) => {
    e.preventDefault();
    resisterInvolved(form, clearFields);
  };
  return (
    <ContainerInvolved>
      <Container sx={styleContainer}>
        <Box>
          <CardMedia
            component="img"
            height="100%"
            image={parte}
            alt="logo meta feedback"
          />
        </Box>
        <Form onSubmit={submitInvolved}>
          <Box sx={styleInputs}>
            <TextInput
              label="Nome"
              variant="filled"
              name="name"
              value={form.name}
              onChange={onChange}
            />
            <TextInput
              label="Empresa"
              variant="filled"
              name="company"
              value={form.company}
              onChange={onChange}
            />
            <TextInput
              label="Cargo"
              variant="filled"
              name="charge"
              value={form.charge}
              onChange={onChange}
            />
          </Box>
          <Box>
            <Button type="submit" sx={buttonFB}>
              Enviar
            </Button>
          </Box>
        </Form>
      </Container>
    </ContainerInvolved>
  );
};
export default Involved;
