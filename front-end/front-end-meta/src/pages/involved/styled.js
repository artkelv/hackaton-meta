import { TextField } from "@mui/material";
import styled from "styled-components";
import background from "../../assets/background.svg";
import { PrimaryColor2 } from "../../constants/colors";

export const ContainerInvolved = styled.div`
  width: 100%;
  height: 100vh;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-size: cover;
`;

export const styleContainer = {
  width: "80%",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-around",
  alignItems: "center",
};
export const Form = styled.form`
  width: 80%;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 50px;
`;
export const styleInputs = {
  display: "flex",
  flexDirection: "column",
  gap: "30px",
  width: "100%",
  alignItems: "center",
};
export const TextInput = styled(TextField)`
  background-color: ${PrimaryColor2};
  border-radius: 5px 5px 0px 0px;
  width: 50%;
  @media screen and (min-width: 320px) and (max-width: 690px) {
    width: 100%;
  }
`;

export const buttonFB = {
  width: `25VW`,
  fontSize: `18px`,
  height: `5vh`,
  color: `${PrimaryColor2}`,
  backgroundColor: `#A1C008`,
  "&:hover": {
    color: `${PrimaryColor2}`,
    backgroundColor: `#A1C008`,
  },
};
