import {Box,FormControl,IconButton,InputBase,InputLabel,MenuItem,Pagination,Select,Stack,Typography,} from "@mui/material";
import React from "react";
import { CardLeaguer } from "../../components/cardLeaguer";
import { Header } from "../../components/header";
import {BoxInputs,Container,ContainerDetail,Footer,Grid,Main,Search,styleFilter,styleInputLabel,} from "./styled";
import SearchIcon from "@mui/icons-material/Search";
import { backgroundColor } from "../../constants/colors";
import FilterListIcon from "@mui/icons-material/FilterList";
import useRequestData from "../../hooks/UseRequestData";
import UseForm from "../../hooks/UseForm";
import UseProtectPage from "../../hooks/UseProtectPage";

const Home = () => {
  const [page, setPage] = React.useState(1);
  const [data] = useRequestData([], `/leaguer?pages=${page}`);
  const { form, onChange } = UseForm({ search: "", order: 1, sortingParameter: "name"});
  UseProtectPage();
  const handleChange = (event, value) => {
    setPage(value);
  };

   return (
    <Container>
      <Header />
      <ContainerDetail>
        <Main>
          <Box>
            <Typography
              variant="subtitle1"
              margin="35px 0px 0px"
              color="neutral"
            >
              Página inicial
            </Typography>
          </Box>

          <BoxInputs>
            <Search
              component="form"
              sx={{ backgroundColor: `${backgroundColor}` }}
 git            >
              <IconButton type="submit" sx={{ p: "10px" }} aria-label="search">
                <SearchIcon />
              </IconButton>

              <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder="O que está procurando?"
                name="search"
                value={form.search}
                onChange={onChange}
              />
            </Search>
            <FormControl sx={{ m: 1, minWidth: 120 }}>
              <InputLabel htmlFor="grouped-select" sx={styleInputLabel}>
                <FilterListIcon />
                Filtrar
              </InputLabel>
              <Select
                defaultValue=""
                id="grouped-select"
                label="Grouping"
                name="order"
                value={form.order}
                onChange={onChange}
              >
                <MenuItem value="">
                  <em>Nenhum</em>
                </MenuItem>
                <MenuItem value={1} sx={styleFilter}>
                  Crescente
                </MenuItem>
                <MenuItem value={-1} sx={styleFilter}>
                  Decrescente
                </MenuItem>
              </Select>
            </FormControl>
          </BoxInputs>

          <Box>
            <Typography variant="subtitle2" color="#636B6F">
              Exibindo {data.leaguers?.length} leaguers
            </Typography>
          </Box>

          <Grid>
            {data.leaguers &&
              data.leaguers
                .filter((league) => {
                  return league.name_leguer
                    ?.toLowerCase()
                    .includes(form.search.toLowerCase());
                })
                .sort((a, b) =>{
                  switch (form.sortingParameter) {
                    case "name":
                    default: 
                      return form.order * a.name.localeCompare(b.name)
                  }
                })
                .map((league) => {
                  return (
                    <CardLeaguer
                      key={league.id}
                      id={league.id}
                      name={league.name_leguer}
                      photo={league.photo}
                      Class={league.ClassName}
                      phase={league.phase_turma}
                      Contributor={league.ContributorsName}
                    />
                  );
                })}
          </Grid>
        </Main>
        <Footer>
          <Stack spacing={2} alignItems="center">
            <Pagination
              count={10}
              onChange={handleChange}
              page={page}
              showFirstButton
              showLastButton
              color="secondary"
            />
          </Stack>
        </Footer>
      </ContainerDetail>
    </Container>
  );
};
export default Home;
