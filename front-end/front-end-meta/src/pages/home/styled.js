import { Box, Paper } from "@mui/material";
import styled from "styled-components";
import { primaryColor } from "../../constants/colors";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
`;

export const ContainerDetail = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 60px;
`;
export const Main = styled(Box)`
  display: grid;
  flex-direction: column;
  width: 80%;
  gap: 20px;
  align-items: stretch;
  @media screen and (min-width: 320px) and (max-width: 439px) {
    width: 95%;
  }
`;

export const BoxInputs = styled(Box)`
  display: flex;
  justify-content: space-between;
  gap: 15px;
  width: 100%;
`;
export const Search = styled(Paper)`
  display: flex;
  align-items: center;
  width: 100%;
  border: 1px solid #636b6f;
`;
export const styleInputLabel = {
  display: "inline-flex",
  justifyContent: "center",
};

export const styleFilter = {
  gap: "5px",
  color: `${primaryColor}`,
};

export const Grid = styled(Box)`
  display: grid;
  width: 100%;
  height: 100%;
  grid-template-columns: repeat(3, 1fr);
  gap: 10px;
  @media screen and (min-width: 320px) and (max-width: 439px) {
    grid-template-columns: repeat(1, 1fr);
    width: 100%;
  }
  @media screen and (min-width: 440px) and (max-width: 891px) {
    grid-template-columns: repeat(1, 1fr);
  }
  @media screen and (min-width: 892px) and (max-width: 1353px) {
    grid-template-columns: repeat(2, 1fr);
  }
`;

export const Footer = styled.footer`
  flex-shrink: 0;
  width: 100%;
  padding: 40px;
  bottom: 0;
`;
