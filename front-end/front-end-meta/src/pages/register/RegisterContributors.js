import React from "react";
import {Container,Form,Input,SubContainer,ContainerH1,ContainerRegister,} from "./styled";
import { Menu } from "../../components/menu/index";
import { Header } from "../../components/header/index";
import { TextField } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Button from "@mui/material/Button";
import UseForm from "../../hooks/UseForm";
import { resisterContributor } from "../../services/requests";
import UseProtectPage from "../../hooks/UseProtectPage";

const RegisterContributors = () => {
  const { form, onChange, clearFields } = UseForm({
    name: "",
    email: "",
    password: "",
    role: "",
  });
  UseProtectPage();

  const submitRegisterContributor = (e) => {
    e.preventDefault();
    resisterContributor(form, clearFields);
  };

  return (
    <ContainerRegister>
      <Header />
      <Container>
        <Menu />
        <SubContainer>
          <Form onSubmit={submitRegisterContributor}>
            <ContainerH1>
              <h1>Cadastrar Colaborador</h1>
            </ContainerH1>
            <Input>
              <TextField
                id="outlined-basic"
                label="Nome"
                variant="outlined"
                name="name"
                onChange={onChange}
                value={form.name}
              />
              <TextField
                id="outlined-basic"
                label="Email"
                variant="outlined"
                name="email"
                onChange={onChange}
                value={form.email}
              />
              <TextField
                type="password"
                id="outlined-basic"
                label="Senha"
                variant="outlined"
                name="password"
                onChange={onChange}
                value={form.password}
              />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Cargo</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Cargo"
                  name="role"
                  onChange={onChange}
                  value={form.role}
                >
                  <MenuItem value="GESTOR">Gestor</MenuItem>
                  <MenuItem value="ADM">Administrador</MenuItem>
                  <MenuItem value="MENTOR">Mentor</MenuItem>
                </Select>
              </FormControl>
            </Input>
            <Button
              variant="contained"
              type={"submit"}
              sx={{
                background: "#122870",
                color: "#F5F5F5",
                width: "40%",
                height: "7%",
              }}
            >
              CADASTRAR
            </Button>
          </Form>
        </SubContainer>
      </Container>
    </ContainerRegister>
  );
};
export default RegisterContributors;
