import styled from "styled-components";
import background from "../../assets/background.svg";

export const ContainerRegister = styled.div`
  width: 100%;
  height: 100vh;
`;
export const Container = styled.div`
  height: 93.2%;
  @media screen and (min-width: 230px) and (max-width: 600px) {
    height: 94%;
  }
  &.dark-mode {
    background-color: #000;
    background: #000;
    @media only screen and (min-width: 300px) and (max-width: 1107px) {
      min-width: 300px;
      left: 10%;
      height: 500px;
    }
  }
`;
export const SubContainer = styled.div`
  display: flex;
  align-items: center;
  justify-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-size: cover;
`;
export const Form = styled.form`
  display: flex;
  background: white;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 80vw;
  height: 80vh;
  border-radius: 9px 9px 9px 9px;
  justify-content: space-evenly;
`;
export const Input = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  height: 35%;
  width: 90%;
  align-items: center;
  gap: 12%;
`;
export const ContainerH1 = styled.div`
  font-family: Open Sans;
  h1 {
    position: relative;
    bottom: 100%;
    color: #122870;
    font-weight: 190px;
  }
`;
export const DivButton = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
export const styleImg = {
  borderRadius: "50%",
  width: "80px",
  margin: "0px 0px 5px",
  cursor: "pointer",
};
