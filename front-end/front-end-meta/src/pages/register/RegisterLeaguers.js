import React from "react";
import { Menu } from "../../components/menu/index";
import { Header } from "../../components/header/index";
import {Container,Form,SubContainer,ContainerH1,ContainerRegister,} from "./styled";
import styled from "styled-components";
import {CardMedia,FormControl,InputLabel,MenuItem,Select,TextField,} from "@mui/material";
import Button from "@mui/material/Button";
import useRequestData from "../../hooks/UseRequestData";
import UseForm from "../../hooks/UseForm";
import { registerLeaguers } from "../../services/requests";
import profilephoto from "../../assets/profilephoto.png";
import { styleImg } from "../profileLeaguer/modalLeaguer/styled";
import UseProtectPage from "../../hooks/UseProtectPage";

const Input = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  width: 75%;
  height: 30%;
  gap: 10%;
`;
const InputTwo = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: flex-end;
`;

const InputFile = styled("input")({
  display: "none",
});

const RegisterLeaguers = () => {
  const [img, setImg] = React.useState(null);
  const [data] = useRequestData([], "/user");
  const ClassData = useRequestData([], "/class");
  UseProtectPage();

  const { form, onChange, clearFields } = UseForm({
    name: "",
    email: "",
    tecnologies: "",
    contributors: "",
    class: "",
    language: "",
  });
  const submitLeaguer = (ev) => {
    ev.preventDefault();
    registerLeaguers(form, clearFields);
  };

  const dowloadImg = (e) => {
    setImg(URL.createObjectURL(e.target.files[0]));
  };

  return (
    <ContainerRegister>
      <Header />
      <Container>
        <Menu />
        <SubContainer>
          <Form onSubmit={submitLeaguer}>
            <ContainerH1>
              <h1>Cadastrar Leaguers</h1>
            </ContainerH1>
            <label htmlFor="icon-button-file">
              <InputFile
                accept=".png,.jpeg,.jpg"
                id="icon-button-file"
                type="file"
                onChange={dowloadImg}
              />
              <CardMedia
                sx={styleImg}
                component="img"
                height="80px"
                image={img !== null ? img : `${profilephoto}`}
                alt="foto leaguer"
              />
            </label>

            <Input>
              <TextField
                name="name"
                value={form.name}
                onChange={onChange}
                id="outlined-basic"
                label="Nome"
                variant="outlined"
              />
              <TextField
                name="email"
                value={form.email}
                onChange={onChange}
                id="outlined-basic"
                label="Email"
                variant="outlined"
                type="email"
              />
              <FormControl sx={{ width: "100%" }}>
                <InputLabel id="demo-simple-select-label">Turma</InputLabel>
                <Select
                  name="class"
                  value={form.class}
                  onChange={onChange}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Turma"
                >
                  {ClassData[0]?.classes &&
                    ClassData[0]?.classes.map((Clas) => {
                      return (
                        <MenuItem key={Clas.id} value={Clas.id}>
                          {Clas.ClassName}
                        </MenuItem>
                      );
                    })}
                </Select>
              </FormControl>
              <TextField
                name="language"
                value={form.language}
                onChange={onChange}
                id="outlined-basic"
                label="Idiomas"
                variant="outlined"
              />
              <TextField
                name="tecnologies"
                value={form.tecnologies}
                onChange={onChange}
                id="outlined-basic"
                label="Tecnologias"
                variant="outlined"
              />
              <FormControl sx={{ width: "100%" }}>
                <InputLabel id="demo-simple-select-label">
                  Gestor ou responsável
                </InputLabel>
                <Select
                  name="contributors"
                  value={form.contributors}
                  onChange={onChange}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Gestor ou responsável"
                >
                  {data.contributors &&
                    data.contributors.map((contributor) => {
                      return (
                        <MenuItem key={contributor.id} value={contributor.id}>
                          {contributor.ContributorsName}
                        </MenuItem>
                      );
                    })}
                </Select>
              </FormControl>
            </Input>
            <InputTwo></InputTwo>
            <Button
              onClick={() => submitLeaguer()}
              type="submit"
              variant="contained"
              sx={{
                background: "#122870",
                color: "#F5F5F5",
                width: "40%",
                height: "7%",
              }}
            >
              CADASTRAR
            </Button>
          </Form>
        </SubContainer>
      </Container>
    </ContainerRegister>
  );
};
export default RegisterLeaguers;
