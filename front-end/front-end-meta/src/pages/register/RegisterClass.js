import React from "react";
import {Container,Form,SubContainer,ContainerH1,ContainerRegister,} from "./styled";
import { Menu } from "../../components/menu/index";
import { Header } from "../../components/header/index";
import styled from "styled-components";
import Button from "@mui/material/Button";
import {FormControl,InputLabel,MenuItem,Select,TextField,} from "@mui/material";
import UseForm from "../../hooks/UseForm";
import { registerClass } from "../../services/requests";
import useRequestData from "../../hooks/UseRequestData";
import UseProtectPage from "../../hooks/UseProtectPage";

const InputOne = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  height: 12%;
`;
const InputTwo = styled.div`
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  height: 12%;
`;

const RegisterClass = () => {
  const [data] = useRequestData([], "/user");
  UseProtectPage();

  const { form, onChange, clearFields } = UseForm({
    name: "",
    phase: "",
    contributors: "",
  });

  const submitClass = (e) => {
    e.preventDefault();
    registerClass(form, clearFields);
  };
  return (
    <ContainerRegister>
      <Header />
      <Container>
        <Menu />
        <SubContainer>
          <Form onSubmit={submitClass}>
            <ContainerH1>
              <h1>Cadastrar Turma</h1>
            </ContainerH1>

            <InputOne>
              <TextField
                name="name"
                value={form.name}
                onChange={onChange}
                sx={{ width: "35%" }}
                id="outlined-basic"
                label="Nome"
                variant="outlined"
              />
              <FormControl sx={{width: "35%"}}>
                <InputLabel id="demo-simple-select-label">Fase</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Fase"
                  name="phase"
                  onChange={onChange}
                  value={form.phase}
                  required
                  >
                  <MenuItem value="Introducao">Introdução</MenuItem>
                  <MenuItem value="Labs">Labs</MenuItem>
                  <MenuItem value="Beta">Beta</MenuItem>
                </Select>
              </FormControl>
            </InputOne>

            <InputTwo>
              <FormControl sx={{ width: "37%" }}>
                <InputLabel id="demo-simple-select-label">
                  Gestor ou responsável
                </InputLabel>
                <Select
                  name="contributors"
                  value={form.contributors}
                  onChange={onChange}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="Gestor ou responsável"
                >
                  {data.contributors &&
                    data.contributors.map((contributor) => {
                      return (
                        <MenuItem value={contributor.id}>
                          {contributor.ContributorsName}
                        </MenuItem>
                      );
                    })}
                </Select>
              </FormControl>
            </InputTwo>

            <Button
              type="submit"
              variant="contained"
              sx={{
                background: "#122870",
                color: "#F5F5F5",
                width: "40%",
                height: "7%",
              }}
            >
              CADASTRAR
            </Button>
          </Form>
        </SubContainer>
      </Container>
    </ContainerRegister>
  );
};
export default RegisterClass;
