import styled from "styled-components";
import { primaryColor } from "../../constants/colors";

export const ContainerHistoric = styled.div`
width: 100%;
height: 100vh;
`
export const styleContainer = {
height: '93.1%',
}
export const styleBox = {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
}
export const boxCard = {
    width: '100%',
}
export const cardStyle = {
    background: 'none',
    maxWidth: "15%",
    color: `${primaryColor}`,
}