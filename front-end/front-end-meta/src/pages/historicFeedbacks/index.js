import { Box, Card, CardActionArea, CardMedia, Container, Typography } from "@mui/material";
import * as React from "react";
import { Header } from "../../components/header/index";
import { Menu } from "../../components/menu";
import UseProtectPage from "../../hooks/UseProtectPage";
import { boxCard, cardStyle, ContainerHistoric, styleBox, styleContainer } from "./styled";

const HistoricFeedbacks = () => {
  UseProtectPage();
  return (
    <ContainerHistoric>
      <Header />
      <div>
        <Menu />
      </div>
      <Container sx={styleContainer}>
        <Box sx={styleBox}>
          <Box>
            <Typography variant="h4" color="primary" sx={{ padding: "20px" }}>
              Histórico de avaliações
            </Typography>
          </Box>
          <Box sx={boxCard}>
            <Card sx={cardStyle}>
                <Typography>Leaguer</Typography>
              <CardActionArea>
                <CardMedia
                    sx={{objectFit: 'cover'}}
                  component="img"
                  height="220"
                  image="https://picsum.photos/200/300"
                  alt="green iguana"
                />
              </CardActionArea>
            </Card>
            
          </Box>
        </Box>
      </Container>
    </ContainerHistoric>
  );
};
export default HistoricFeedbacks;
