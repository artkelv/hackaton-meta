import styled from "styled-components";
import { PrimaryColor2 } from "../../constants/colors";

export const ContainerForm = styled.div`
  background: #0056a8;
  width: 100%;
  height: 100%;
`;
export const Logo = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  height: 15vh;
  img {
    margin-top: 20px;
    width: 230px;
  }
`;
export const FormsGroup = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 20px;
  margin: 50px 0px 0px;
`;

export const Forms = styled.div`
  width: 90%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: center;
  gap: 20px;
`;
export const FormsSelect = styled.div`
  width: 90%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: center;
  gap: 10px;
`;

export const ContainerOne = styled.div`
  width: 80%;
`;
export const Containertwo = styled.div`
  width: 80%;
`;
export const Containertree = styled.div`
  width: 80%;
`;
export const boxButton = {
  width: "100%",
  display: "flex",
  justifyContent: "center",
  padding: "50px 0px 70px",
};

export const styleButton = {
  color: `${PrimaryColor2}`,
  fontSize: "18px",
  backgroundColor: "#A1C008",
  width: "35%",
  height: "50px",
  "&:hover": {
    color: `${PrimaryColor2}`,
    backgroundColor: "#A1C008",
  },
};
