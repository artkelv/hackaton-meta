import { Box,Button,FormControl,MenuItem,Select,TextField,Typography,} from "@mui/material";
import * as React from "react";
import parte from "../../assets/parte.svg";
import useRequestData from "../../hooks/UseRequestData";
import {Logo,ContainerForm,ContainerOne,Containertwo,Containertree,FormsGroup,Forms,styleButton,boxButton,} from "./styled";

const Form = () => {
  const [data] = useRequestData([], `/question`);

  const requests =
    data.questions &&
    data.questions?.map((quests) => {
      return (
        <ContainerOne key={quests.id}>
          <TextField
            label={quests.question}
            variant="filled"
            sx={{
              background: "white",
              width: "100%",
              borderRadius: "5px 5px 0px 0px",
            }}
          />
        </ContainerOne>
      );
    });

  const requests2 =
    data.questions &&
    data.questions?.map((quests) => {
      return (
        <Containertwo key={quests.id}>
          <Typography variant="p" sx={{ color: "#fff" }}>
            {quests.question}
          </Typography>
          <FormControl sx={{ width: "100%" }}>
            <Select sx={{ background: "#F0F0F0" }}>
              <MenuItem value="">
                <em>Nenhum</em>
              </MenuItem>
              <MenuItem value={10}>Pode ser desenvolvido</MenuItem>
              <MenuItem value={20}>Regular</MenuItem>
              <MenuItem value={20}>Acima da média</MenuItem>
            </Select>
          </FormControl>

          <FormControl
            sx={{
              background: "white",
              width: "100%",
              marginTop: "5px",
              borderRadius: "5px 5px 0px 0px",
            }}
          >
            <TextField
              id="outlined-multiline-static"
              label="Descrição"
              variant="filled"
              multiline
              rows={4}
            />
          </FormControl>
        </Containertwo>
      );
    });

  const requests3 =
    data.questions &&
    data.questions?.map((quests) => {
      return (
        <Containertree key={quests.id}>
          <FormControl
            sx={{
              background: "white",
              width: "100%",
              borderRadius: "5px 5px 0px 0px",
            }}
          >
            <TextField
              id="outlined-multiline-static"
              label={quests.question}
              variant="filled"
              multiline
              rows={4}
            />
          </FormControl>
        </Containertree>
      );
    });

  return (
    <ContainerForm>
      <Logo>
        <img src={parte} alt="part" />
      </Logo>

      <FormsGroup>
        <Forms>{requests?.slice(0, 6)}</Forms>

        <Forms>{requests2?.slice(6, 16)}</Forms>

        <Forms>{requests3?.slice(16, 19)}</Forms>
      </FormsGroup>

      <Box sx={boxButton}>
        <Button sx={styleButton}>Enviar</Button>
      </Box>
    </ContainerForm>
  );
};
export default Form;
