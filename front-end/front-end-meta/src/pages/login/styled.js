import styled from "styled-components";
import background from "../../assets/background.svg";

export const ContainerLogin = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-items: center;
  align-items: end;
  width: 100%;
  height: 100vh;
  background-image: url(${background});
  background-repeat: no-repeat;
  background-size: cover;
  @media only screen and (min-width: 320px) and (max-width: 1050px) {
    grid-template-columns: 1fr;
  }
  .logo {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    background: none;
  }
`;
export const LogoMeta = styled.img`
  background: transparent;
  @media only screen and (min-width: 320px) and (max-width: 1000px) {
    width: 200px;
    margin-top: 3%;
  }
`;
export const Containerform = styled.div`
  display: flex;
  background: white;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 65%;
  height: 600px;
  border-radius: 9px 9px 0px 0px;

  @media only screen and (min-width: 300px) and (max-width: 1107px) {
    min-width: 300px;
    left: 10%;
    height: 500px;
  }
`;
export const ImgName = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  gap: 10px;
  font-family: Open Sans;
  margin-bottom: 20%;
  @media only screen and (min-width: 837px) and (max-width: 831px) {
    width: 100px;
  }
`;
export const DivInputButton = styled.div`
  padding: 0px;
  display: flex;
  flex-direction: column;
  gap: 40px;
`;
export const Inputs = styled.input`
  padding: 5%;
  width: 99%;
  height: 15%;
  border-radius: 5px;
  margin-bottom: 17px;
  border: 1px solid #636b6f;

  @media only screen and (min-width: 300px) and (max-width: 1179px) {
    width: 250px;
  }
  @media only screen and (min-width: 804px) and (max-width: 1048px) {
    width: 500px;
    height: 20px;
  }
  @media only screen and (min-width: 500px) and (max-width: 860px) {
    width: 250px;
    height: 50px;
  }
`;
export const Button = styled.button`
  background: #122870;
  color: white;
  border-style: outset;
  border-color: #122870;
  height: 36px;
  width: 350px;
  font: bold 15px Open Sans;
  text-shadow: none;
  margin-top: 7px;
  :hover {
    cursor: pointer;
  }
  @media only screen and (min-width: 300px) and (max-width: 1179px) {
    width: 250px;
  }
  @media only screen and (min-width: 804px) and (max-width: 1048px) {
    width: 500px;
  }
  @media only screen and (min-width: 500px) and (max-width: 860px) {
    width: 250px;
  }
`;

export const styleButton = {
  background: "#122870",
  color: "#F5F5F5",
  width: "40%",
  height: "99%",
  ":hover": { cursor: "pointer" },
};
