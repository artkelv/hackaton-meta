import React from "react";
import {
  ContainerLogin,
  LogoMeta,
  Containerform,
  ImgName,
  DivInputButton,
  Button,
  styleButton,
} from "./styled";
import parte from "../../assets/parte.svg";
import nomemeta from "../../assets/nomemeta.jpeg";
import { FormControl, IconButton, Input, InputAdornment, InputLabel, TextField } from "@mui/material";
import UseForm from "../../hooks/UseForm";
import { onSubmitLogin } from "../../services/requests";
import { useNavigate } from "react-router-dom";
import { Visibility, VisibilityOff } from "@mui/icons-material";

const Login = () => {
  const navigate = useNavigate();
  const { form, onChange, clearFields } = UseForm({ email: "", password: "" });
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });
  
  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const submitLogin = (e) => {
    e.preventDefault();
    onSubmitLogin(form, clearFields, navigate);
  };

  return (
    <ContainerLogin>
      <div className="logo">
        <LogoMeta src={parte} alt="part" />
      </div>
      <Containerform>
        <form onSubmit={submitLogin}>
          <ImgName>
            <img src={nomemeta} alt="nome-meta" />
            <h1> Bem-Vindo!</h1>
            <p>Faça seu Login</p>
          </ImgName>
          <DivInputButton>
            <TextField
              id="outlined-basic"
              label="Faça seu Login"
              variant="standard"
              name="email"
              value={form.email}
              onChange={onChange}
            />
           
            <FormControl  variant="standard">
            <InputLabel htmlFor="standard-adornment-password">Digite sua senha</InputLabel>
            <Input
              name="password"
              id="standard-adornment-password"
              type={values.showPassword ? 'text' : 'password'}
              value={form.password}
              onChange={onChange}
              endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {values.showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
            <Button variant="contained" sx={styleButton} type="submit">
              ENTRAR
            </Button>
          </DivInputButton>
        </form>
      </Containerform>
    </ContainerLogin>
  );
};
export default Login;
