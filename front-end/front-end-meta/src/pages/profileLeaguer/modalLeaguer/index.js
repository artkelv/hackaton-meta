import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import GlobalStateContext from "../../../global/GlobalStateContext";
import { CardMedia, TextField } from "@mui/material";
import { style,buttonSave,BoxInputs,BoxModalStyle,styleImg,} from "./styled";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { styled } from "@mui/material/styles";
import profilephoto from "../../../assets/profilephoto.png";
import useRequestData from "../../../hooks/UseRequestData";
import UseForm from "../../../hooks/UseForm";
import { editLeaguer } from "../../../services/requests";

const Input = styled("input")({
  display: "none",
});

export const ModalLeaguer = ({ id, open, setOpen }) => {
  const {setVersion} = React.useContext(GlobalStateContext);
  const handleClose = () => setOpen(false);
  const [img, setImg] = React.useState(null);
  const [data] = useRequestData([], "/user");
  const ClassData = useRequestData([], "/class");

  const dowloadImg = (e) => {
    setImg(URL.createObjectURL(e.target.files[0]));
  };

  const { form, onChange,clearFields} = UseForm({
    name: "",
    email: "",
    tecnologies: "",
    contributors: "",
    class_id: "",
    language:"",
  });

  const Edit=(ev)=>{
    ev.preventDefault();
    editLeaguer(id, form, clearFields, updateState);
    handleClose();
  }

  const updateState = () =>
    setVersion((version) => version + 1);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <BoxModalStyle sx={style}>
          <label htmlFor="icon-button-file">
            <Input
              accept=".png,.jpeg,.jpg"
              id="icon-button-file"
              type="file"
              onChange={dowloadImg}
            />
            <CardMedia
              sx={styleImg}
              component="img"
              height="80px"
              image={img !== null ? img : `${profilephoto}`}
              alt="foto leaguer"
            />
          </label>
          <BoxInputs>
            <TextField
              name="name"
              value={form.name}
              onChange={onChange}
              id="outlined-basic"
              label="Nome"
              variant="outlined"
            />
            <FormControl sx={{ width: "100%" }}>
              <InputLabel id="demo-simple-select-label">Gestor ou responsavel</InputLabel>
              <Select
                name="contributors"
                value={form.contributors}
                onChange={onChange}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Gestor ou responsavel"
              >
                {data.contributors &&
                  data.contributors.map((contri) => {
                    return (
                      <MenuItem key={contri.id} value={contri.id}>
                        {contri.ContributorsName}
                      </MenuItem>
                    );
                  })}
                  
              </Select>
            </FormControl>
            <TextField 
             name="email"
             value={form.email}
             onChange={onChange}
             id="outlined-basic"
             label="E-mail" 
             variant="outlined" />
            <TextField
            name="tecnologies"
            value={form.tecnologies}
            onChange={onChange}
              id="outlined-basic"
              label="Tecnologias"
              variant="outlined"
            />

            <FormControl sx={{ width: "100%" }}>
              <InputLabel id="demo-simple-select-label">Turma</InputLabel>
              <Select
                name="class_id"
                value={form.class_id}
                onChange={onChange}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Turma"
              >
                {ClassData[0]?.classes &&
                  ClassData[0]?.classes.map((Clas) => {
                    return (
                      <MenuItem key={Clas.id} value={Clas.id}>
                        {Clas.ClassName}
                      </MenuItem>
                    );
                  })}
              </Select>
            </FormControl>

            <TextField 
            name="language"
            value={form.language}
            onChange={onChange}
            id="outlined-basic" 
            label="Idiomas"
             variant="outlined" />
          </BoxInputs>

          <Box>
            <Button type={"submit"}onClick={Edit} sx={buttonSave}>
              Salvar
            </Button>
          </Box>
        </BoxModalStyle>
      </Modal>
    </div>
  );
};
