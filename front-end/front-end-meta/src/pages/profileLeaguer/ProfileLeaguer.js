import { Button, CardMedia, Typography } from "@mui/material";
import React from "react";
import { Header } from "../../components/header";
import GlobalStateContext from "../../global/GlobalStateContext";
import { ModalLeaguer } from "./modalLeaguer";
import { ModalFeedback } from "../../components/modalFeedBack/index";
import {BoxDetail,BoxLeaguer,ButtonFeedBack,ContainerLeaguer,ContainerPL,editButton,feedBackButton,TypeInfo,} from "./styled";
import GroupsIcon from "@mui/icons-material/Groups";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import AssignmentIndIcon from "@mui/icons-material/AssignmentInd";
import HistoryIcon from "@mui/icons-material/History";
import useRequestData from "../../hooks/UseRequestData";
import { useParams } from "react-router-dom";
import UseProtectPage from "../../hooks/UseProtectPage";

const ProfileLeaguer = () => {
  const params = useParams();
  const { version } =React.useContext(GlobalStateContext);
  const [open, setOpen] = React.useState(false);
  const [openFeedback, setOpenFeedback] = React.useState(false);
  const [data] = useRequestData([], `/leaguer/${params.id}`,version);
  const handleOpen = () => setOpen(true);
  const handleOpenFeedback = () => setOpenFeedback(true);
  UseProtectPage();
  
  return (
    <ContainerPL>
      <Header />
      <ContainerLeaguer>
        <BoxLeaguer>
          <CardMedia
            sx={{ borderRadius: "50%", width: "200px", marginTop: "20px" }}
            component="img"
            height="200px"
            image="https://picsum.photos/200/300.jpg"
            alt="foto leaguer"
          />
          <Button sx={editButton} color="primary2" onClick={handleOpen}>
            Editar perfil
          </Button>

          <Typography variant="h3" color="primary">
            {data.leaguer?.name_leguer}
          </Typography>
        </BoxLeaguer>

        <BoxDetail>
          <TypeInfo variant="p" fontweight="semi-bold">
            Email: {data.leaguer?.email}
          </TypeInfo>

          <TypeInfo variant="p" fontweight="semi-bold">
            <GroupsIcon />
            {data.leaguer?.ClassName}
          </TypeInfo>

          <TypeInfo variant="p" fontweight="semi-bold">
            Tecnologias: {data.leaguer?.tecnologies}
          </TypeInfo>

          <TypeInfo variant="p" fontweight="semi-bold">
            <MenuBookIcon />
            {data.leaguer?.phase_turma}
          </TypeInfo>

          <TypeInfo variant="p" fontweight="semi-bold">
            Idiomas:
            {data.leaguer?.language === null
              ? "Português"
              : data.leaguer?.language}
          </TypeInfo>

          <TypeInfo variant="p" fontweight="semi-bold">
            <AssignmentIndIcon />
            {data.leaguer?.ContributorsName}
          </TypeInfo>
        </BoxDetail>

        <ButtonFeedBack>
          <Button
            variant="contained"
            sx={feedBackButton}
            onClick={handleOpenFeedback}
          >
            Nova avaliação
          </Button>
          <Button
            variant="contained"
            sx={feedBackButton}
            onClick={handleOpenFeedback}
          >
            <HistoryIcon sx={{ marginRight: "5px" }} />
            Historico
          </Button>
        </ButtonFeedBack>
      </ContainerLeaguer>
      <ModalLeaguer id={params.id} open={open} setOpen={setOpen}/>
      <ModalFeedback openFeedback={openFeedback} setOpenFeedback={setOpenFeedback} email={data.leaguer?.email}/>
    </ContainerPL>
  );
};
export default ProfileLeaguer;
