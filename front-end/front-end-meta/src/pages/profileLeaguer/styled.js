import { Box, Container, Typography } from "@mui/material";
import styled from "styled-components";
import { primaryColor, secondaryColor } from "../../constants/colors";

export const ContainerPL = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${secondaryColor};
`;

export const ContainerLeaguer = styled(Container)`
  width: 80%;
  height: 93.2%;
  background-color: #fff;
`;
export const BoxLeaguer = styled(Box)`
  width: 100%;
  height: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  gap: 20px;
  margin: 0px 0px 20px 0px;
`;
export const BoxDetail = styled(Box)`
  width: 100%;
  height: 40%;
  display: grid;
  align-items: center;
  justify-items: center;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: repeat(3, 50px);

  @media screen and (min-width: 570px) and (max-width: 800px) {
    justify-items: start;
  }
  @media screen and (min-width: 220px) and (max-width: 669px) {
    grid-template-columns: 1fr;
    justify-items: start;
  }
`;
export const TypeInfo = styled(Typography)`
  color: ${primaryColor};
  display: inline-flex;
  gap: 5px;
`;
export const ButtonFeedBack = styled(Box)`
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
`;
export const editButton = {
  backgroundColor: `${primaryColor}`,
  "&:hover": {
    backgroundColor: `${primaryColor}`,
  },
};
export const feedBackButton = {
  marginLeft: "10px",
  backgroundColor: "#D55E00",
  color: "#fff",
  "&:hover": {
    backgroundColor: "#D55E00",
    color: "#fff",
  },
};
