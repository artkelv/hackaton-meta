import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import React from "react";
import { Header } from "../../../components/header";
import { Menu } from "../../../components/menu";
import { NameHistoric, NamePeople, AcordionDivOne } from "./styled";

const historic = [
  {
    nome: "José",
    id: 1,
  },
  {
    nome: "Gledson",
    id: 2,
  },
];

const Historic = () => {
  const mapHistoric = historic.map((his) => {
    return (
      <div>
        <NamePeople key={his.id}>
          <h2>{his.nome}</h2>
        </NamePeople>
        <AcordionDivOne>
          <Accordion sx={{ width: "80%" }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel2a-content"
              id="panel2a-header"
            >
              <Typography>Ver feedback</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>Perguntas e respostas</Typography>
            </AccordionDetails>
          </Accordion>
        </AcordionDivOne>
      </div>
    );
  });

  return (
    <div>
      <Header />
      <Menu />
      <NameHistoric>
        <h1>Historico</h1>
      </NameHistoric>
      {mapHistoric.slice(0, 2)}
    </div>
  );
};
export default Historic;
