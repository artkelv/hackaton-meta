import styled from "styled-components";

export const NameHistoric = styled.div`
  height: 100%;
  display: flex;
  flex-direction: center;
  align-items: center;
  justify-content: center;
  h1 {
    color: #2d2d2d;
    margin-bottom: 5%;
  }
`;
export const NamePeople = styled.div`
  display: flex;
  justify-content: center;
  h2 {
    width: 80%;
    color: #2d2d2d;
  }
`;
export const AcordionDivOne = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
