import styled from "styled-components";
import Box from "@mui/material/Box";
import { Card, Paper, Typography } from "@mui/material";
import { primaryColor, PrimaryColor2 } from "../../constants/colors";

export const Container = styled(Box)`
  width: 100%;
  height: 100vh;
`;
export const BoxHome = styled(Box)`
  width: 100%;
  height: 93.2%;
`;
export const BoxInfosLeaguer = styled(Box)`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  align-items: center;
`;
export const BoxQuantity = styled(Box)`
  width: 80%;
  height: 30%;
`;
export const InfosMetaLeague = styled(Typography)`
  width: 100%;
  display: inline-flex;
  justify-content: center;
  background-color: ${primaryColor};
  color: #fff;
  border-radius: 20px 20px 0px 0px;
  padding: 5px;
`;
export const BoxMetaLeague = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 90%;
  background-color: #fff;
  overflow-x: auto;
  scroll-behavior: smooth;
`;

export const CardMetaLeague = styled(Card)`
  min-width: 170px;
  width: 200px;
  height: 80%;
  display: inline-flex;
  flex-direction: column;
  justify-content: center;
  
  align-items: center;
  margin: 0px 15px 0px;

  h2 {
    color: ${PrimaryColor2};
  }
  div {
    display: inline-flex;
    align-items: flex-end;
    align-items: stretch;
    gap: 10px;
    p {
      color: ${PrimaryColor2};
      display: inline;
      font-size: 25px;
    }
  }
`;
export const cardStyle = {
  backgroundColor: `${primaryColor}`,
};

export const TableLeague = styled(Box)`
  width: 80%;
  height: 60%;
  border-radius: 10px;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 20px;
  .search {
    width: 90%;
    display: flex;
    justify-content: center;
    gap: 10px;
    margin: 40px 0px 0px;
  }
  .table {
    width: 100%;
    height: 100%;
    align-items: center;
    display: flex;
    justify-content: center;
    align-items: flex-start;
  }
`;

export const Search = styled(Paper)`
  display: flex;
  align-items: center;
  width: 100%;
  border: 1px solid #636b6f;
`;
