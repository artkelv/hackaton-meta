import React from "react";
import { CustomizedTables } from "../../components/costumizedTable";
import { Header } from "../../components/header";
import { Menu } from "../../components/menu";
import {
  BoxHome,
  BoxInfosLeaguer,
  BoxMetaLeague,
  BoxQuantity,
  CardMetaLeague,
  Container,
  InfosMetaLeague,
  TableLeague,
  cardStyle,
} from "./styled";
import GroupsIcon from "@mui/icons-material/Groups";
import UseProtectPage from "../../hooks/UseProtectPage";
import useRequestData from "../../hooks/UseRequestData";

const ProfileContributor = () => {
  const [contributors] = useRequestData([], `/user`);

  const lengthMentor = contributors.contributors?.filter(
    (contributor) => contributor.role === "MENTOR"
  );
  const lengthGestor = contributors.contributors?.filter(
    (contributor) => contributor.role === "GESTOR"
  );

  UseProtectPage();

  return (
    <Container>
      <Header />
      <BoxHome>
        <div>
          <Menu />
        </div>
        <BoxInfosLeaguer>
          <BoxQuantity>
            <InfosMetaLeague>Meta league</InfosMetaLeague>
            <BoxMetaLeague>
              <CardMetaLeague sx={cardStyle}>
                <div>
                  <GroupsIcon color="primary2" fontSize="large" />
                  <p>08</p>
                </div>
                <h2> Leaguer </h2>
              </CardMetaLeague>
              <CardMetaLeague sx={cardStyle}>
                <div>
                  <GroupsIcon color="primary2" fontSize="large" />
                  <p>08</p>
                </div>
                <h2> Turmas </h2>
              </CardMetaLeague>
              <CardMetaLeague sx={cardStyle}>
                <div>
                  <GroupsIcon color="primary2" fontSize="large" />
                  <p>{lengthMentor?.length}</p>
                </div>
                <h2> Mentores </h2>
              </CardMetaLeague>
              <CardMetaLeague sx={cardStyle}>
                <div>
                  <GroupsIcon color="primary2" fontSize="large" />
                  <p>{lengthGestor?.length}</p>
                </div>
                <h2> Gestores </h2>
              </CardMetaLeague>
            </BoxMetaLeague>
          </BoxQuantity>
          <TableLeague>
            <div className="table">
              <CustomizedTables />
            </div>
          </TableLeague>
        </BoxInfosLeaguer>
      </BoxHome>
    </Container>
  );
};
export default ProfileContributor;
