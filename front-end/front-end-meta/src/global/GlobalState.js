import React from "react";
import GlobalStateContext from "./GlobalStateContext";

const GlobalState = (props) => {
    const [version, setVersion] = React.useState(1);
    const data = {
        version,
        setVersion
    }
    
    return(
        <GlobalStateContext.Provider value={ data }>
        {props.children}
        </GlobalStateContext.Provider>
    )
}

export default GlobalState;
